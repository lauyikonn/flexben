// Require the module
var channelId = 'sound';
var deviceToken = null;
var API = require('api');
var fcm = require('firebase.cloudmessaging');

var subscribeToChannel = function(channelId){
	if(OS_IOS){
		return;
	}
	console.log("delete "+channelId+" ");
	fcm.deleteNotificationChannel(channelId);
	
	//alert("subscribeToChannel "+channel+" "+deviceToken);
	/*var channel = Ti.Android.NotificationManager.createNotificationChannel({
        id: channelId,
        name: channelId,
        importance: Ti.Android.IMPORTANCE_HIGH,
        sound: "default",
        enableLights: true,
        enableVibration: true,
        showBadge: true
    });*/
   setTimeout(function(){
   	console.log("subscribeToChannel "+channelId+" ");
    var channel = fcm.createNotificationChannel({
         sound: 'default',
         channelId: channelId,
         channelName: channelId,
         importance: 'high'
     });
      fcm.notificationChannel = channel;
   }, 1000);
    
    // if you use a custom id you have to set the same to the `channelId` in you php send script!

    
};

exports.subscribeToChannel = subscribeToChannel;

exports.unsubscribeToAll = function(ex){
	if(OS_IOS){
		return;
	}
	console.log("unsubscribeToChannel "+channelId+" "+deviceToken);
	//alert("subscribeToChannel "+channel+" "+deviceToken);

	Cloud.PushNotifications.unsubscribeToken({
        device_token : deviceToken,
    }, function(e) {
		if (e.success) {
	        console.log("unsubscribe with deviceToken "+deviceToken);
	    } else {
	        console.log('Error:\n' +
	            ((e.error && e.message) || JSON.stringify(e)));
	    }
	    ex.callback();
    });
};


exports.pushNotification = function() {
   
var core = require('firebase.core');

// Configure core module (required for all Firebase modules).
if(OS_ANDROID){
	core.configure({
	    GCMSenderID: '1015135996961',
	    googleAppID: '1:1015135996961:android:eacf489780c200fe102760', // Differs between Android and iOS.
	    // file: 'GoogleService-Info.plist' // If using a plist (iOS only).
	});
}else{
	core.configure({
	    GCMSenderID: '566539754482',
	    googleAppID: '1:989169531572:ios:189f188f65a1c72047f5e1', // Differs between Android and iOS.
	    file: 'GoogleService-Info.plist' // If using a plist (iOS only).
	});
}


// Important: The cloud messaging module has to imported after (!) the configure()
// method of the core module is called

// Called when the Firebase token is registered or refreshed.
fcm.addEventListener('didRefreshRegistrationToken', function(e) {
    
    console.log("didRefreshRegistrationToken");
    deviceToken = e.fcmToken;
    var u_id = Ti.App.Properties.getString("u_id");
    console.log("u_id "+u_id);
    console.log(e.fcmToken);
    var params = {};
		params["u_id"] = u_id;
		params["app_version"] = Titanium.App.version;
		params["device_os"] = Titanium.Platform.osname+" "+Titanium.Platform.version;
		params["device_model"] = Titanium.Platform.model+" "+Titanium.Platform.ostype;
		params["device_macaddress"] = Titanium.Platform.macaddress;
		params["device_token"] = e.fcmToken;
		
    API.callByPost({url: "api/validateUser", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
		}, onfinish: function(){
		}
	});
    
});

// Called when direct messages arrive. Note that these are different from push notifications.
fcm.addEventListener('didReceiveMessage', function(e) {
    console.log("didReceiveMessage "+e.message);
    console.log(e.message);
    console.log(e.message.data.eventName+" "+e.message.data.params);
	Ti.App.fireEvent(e.message.data.eventName, e.message.data.params);
});

// Android-only: For configuring custom sounds and importance for the generated system
// notifications when app is in the background
if (OS_ANDROID) {
	console.log("delete general");
	 fcm.deleteNotificationChannel('general');
	 setTimeout(function(){
	 	console.log("create general");
	 	var channel = fcm.createNotificationChannel({
	         sound: 'default',
	         channelId: 'general',
	         channelName: 'general',
	         importance: 'high',
	     });
	     fcm.notificationChannel = channel;
	 }, 1000);
     
    
/*
    var channel = Ti.Android.NotificationManager.createNotificationChannel({
        id: 'general',
        name: 'general',
        importance: Ti.Android.IMPORTANCE_HIGH,
        sound: "doorbell.wav",
        enableLights: true,
        enableVibration: true,
        showBadge: true
    });*/
    // if you use a custom id you have to set the same to the `channelId` in you php send script!

    

    // display last data:
    console.log("last data");
    console.log(fcm.lastData);
    Ti.App.fireEvent(fcm.lastData.eventName, fcm.lastData.params);
    fcm.registerForPushNotifications();
} else {
	// iOS
	// Listen to the notification settings event
	Ti.App.iOS.addEventListener('usernotificationsettings', function eventUserNotificationSettings() {
	  // Remove the event again to prevent duplicate calls through the Firebase API
	  Ti.App.iOS.removeEventListener('usernotificationsettings', eventUserNotificationSettings);

	  // Register for push notifications
	  Ti.Network.registerForPushNotifications({
	    success: function (e) { console.log("notification register success"); console.log(e); console.log('FCM-Token', fcm.fcmToken); 
	    var u_id = Ti.App.Properties.getString("u_id");
	    API.callByPost({url: "api/updateDeviceToken", params: {u_id: u_id, device_id: e.deviceToken}}, {onload: 
	    	function(responseText){
	    		//var result = JSON.parse(responseText);
	    		console.log(responseText);
	    	}
	    });
	    },
	    error: function () { console.log("notification register fail"); },
	    callback: function (e) { console.log("notification callback");
	    console.log(e); 
	    console.log(e.data);
	    Ti.App.fireEvent(e.data.eventName, e.data.params);
	    } // Fired for all kind of notifications (foreground, background & closed)
	  });
	});

	// Register for the notification settings event
	Ti.App.iOS.registerUserNotificationSettings({
	  types: [
	    Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
	    Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
	    Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
	  ]
	});
}

// Register the device with the FCM service.


// Check if token is already available.
if (fcm.fcmToken) {
    console.log('FCM-Token', fcm.fcmToken);
} else {
    console.log('Token is empty. Waiting for the token callback ...');
}

// Subscribe to a topic.
fcm.subscribeToTopic('testTopic');

};

/* below is old*/

var loginUser = function(ex){
    // Log in to Arrow
    var u_id = Ti.App.Properties.getString('dr_id') || "";
    console.log(u_id+" uid here");
    Cloud.Users.login({
        login: u_id,
        password: '123456'

    }, function (e) {
        if (e.success) {
            console.log('Login successful');
            
            ex.callback();
        } else {
        	console.log(e.code+" error code ");
        	if(true){
        		Cloud.Users.create({
				    first_name: u_id,
				    username: u_id,
				    password: '123456',
				    password_confirmation: '123456'
				}, function (e) {
				    if (e.success) {
				        ex.callback();
				    } else {
				        console.log('Error:\n' +
				            ((e.error && e.message) || JSON.stringify(e)));
				    }
	        	  });
            console.log('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
        }
        }
    });
};
exports.loginUser = loginUser;

exports.logoutUser = function(callback) {
	Cloud.Users.logout(function (e) {
	    if (e.success) {
	        console.log('Success: Logged out');
	    } else {
	        console.log('Error:\n' +
	            ((e.error && e.message) || JSON.stringify(e)));
	    }
	});
};

exports.pushNotification_old = function(callback) {
    if (OS_ANDROID) {
        var CloudPush = require('ti.cloudpush');
        CloudPush.retrieveDeviceToken({
            success : deviceTokenSuccess,
            error : deviceTokenError
        });

        // Process incoming push notifications
        CloudPush.addEventListener('callback', function(e) {

            var payload = JSON.parse(e.payload);
            if (payload) {

                callback({
                    error : false,
                    data : payload
                });

            }

        });

        CloudPush.addEventListener('trayClickLaunchedApp', function(e) {
            push_redirect = true;
            var payload = JSON.parse(e.payload);
            if (payload) {

                callback({
                    error : false,
                    data : payload
                });

            }

        });

        CloudPush.addEventListener('trayClickFocusedApp', function(e) {
          push_redirect = true;
            /*
            var payload = JSON.parse(e.payload);
            if (payload) {

                callback({
                    error : false,
                    data : payload
                });

            }*/
        });
    }

    if (OS_IOS) {

        if (Ti.Platform.name === "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {

            // Wait for user settings to be registered before registering for push notifications
            Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {

                // Remove event listener once registered for push notifications
                Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);

                Ti.Network.registerForPushNotifications({
                    success : deviceTokenSuccess,
                    error : deviceTokenError,
                    callback : receiveIOSPush
                });
            });

            // Register notification types to use
            Ti.App.iOS.registerUserNotificationSettings({
                types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
            });
        }

        // For iOS 7 and earlier
        else {

            Ti.Network.registerForPushNotifications({
                // Specifies which notifications to receive
                types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
                success : deviceTokenSuccess,
                error : deviceTokenError,
                callback : receiveIOSPush
            });
        }

        function receiveIOSPush(e) {

            callback({
                error : false,
                data : e.data
            });

        }

    }

    // Save the device token for subsequent API calls
    function deviceTokenSuccess(e) {

        deviceToken = e.deviceToken;
		console.log(deviceToken+' deviceTokenSuccess');
        Titanium.App.Properties.setString('deviceToken', deviceToken);
        var channel = Ti.App.Properties.getString('category')||"";
        subscribeToChannel(channel);
        API.updateNotificationToken();
        Cloud.PushNotifications.subscribeToken({
            device_token : Titanium.App.Properties.getString('deviceToken'),
            channel : channelId,
            type : Ti.Platform.name == 'android' ? 'android' : 'ios',
        }, function(e) {


        });

        // Reset the badge if needed
        Cloud.PushNotifications.resetBadge({
            device_token : Titanium.App.Properties.getString('deviceToken')
        }, function(e) {


        });

    }

    function deviceTokenError(e) {
    	console.log("deviceTokenError");
		console.log(e);

        callback({
            error : true,
            message : e.error
        });

    }

};
