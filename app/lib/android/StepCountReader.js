
const Calendar = require("java.util.Calendar");
const TimeZone = require("java.util.TimeZone");
const HealthConstants = require("com.samsung.android.sdk.healthdata.HealthConstants");
const HealthData = require("com.samsung.android.sdk.healthdata.HealthData");
const HealthDataResolver = require("com.samsung.android.sdk.healthdata.HealthDataResolver");
//const AggregateRequest = require("com.samsung.android.sdk.healthdata.HealthDataResolver.AggregateRequest");
import AggregateRequest from 'com.samsung.android.sdk.healthdata.HealthDataResolver.AggregateRequest';
import TimeGroupUnit from 'com.samsung.android.sdk.healthdata.HealthDataResolver.AggregateRequest.TimeGroupUnit';
import AggregateFunction from 'com.samsung.android.sdk.healthdata.HealthDataResolver.AggregateRequest.AggregateFunction';
//const TimeGroupUnit = require("com.samsung.android.sdk.healthdata.HealthDataResolver.AggregateRequest.TimeGroupUnit"); 
//const AggregateFunction = AggregateRequest.AggregateFunction;
const Filter = require("com.samsung.android.sdk.healthdata.HealthDataResolver.Filter");
const ReadRequest = require("com.samsung.android.sdk.healthdata.HealthDataResolver.ReadRequest");
const SortOrder = require("com.samsung.android.sdk.healthdata.HealthDataResolver.SortOrder");
const HealthDataStore = require("com.samsung.android.sdk.healthdata.HealthDataStore");
const HealthDataUtil = require("com.samsung.android.sdk.healthdata.HealthDataUtil");

function StepCountReader(store,observer) {
    this.store = store;
    this.observer = observer;
    this.TODAY_START_UTC_TIME = getTodayStartUtcTime();
    this.ONE_DAY = 24 * 60 * 60 * 1000;
	this.PROPERTY_TIME = "day_time";
    this.PROPERTY_COUNT = "count";
    this.PROPERTY_BINNING_DATA = "binning_data";
    this.ALIAS_TOTAL_COUNT = "count";
    this.ALIAS_DEVICE_UUID = "deviceuuid";
    this.ALIAS_BINNING_TIME = "binning_time";
}
 
StepCountReader.prototype.StepCountObserver = function() {
    
};

StepCountReader.prototype.requestDailyStepCount = function(startTime) {
    if (startTime >= this.TODAY_START_UTC_TIME) {
        // Get today step count
        console.log("get today");
        readStepCount(startTime);
    } else {
    	console.log("get history");
        // Get historical step count
        //readStepDailyTrend(startTime);
    }
};

function readStepCount(startTime) {
    // Get sum of step counts by device
    console.log(AggregateRequest.Builder); 
    var request = new AggregateRequest.Builder();
    /*
    request = new Builder.build()
            .setDataType(HealthConstants.StepCount.HEALTH_DATA_TYPE)
            .addFunction(AggregateFunction.SUM, HealthConstants.StepCount.COUNT, this.ALIAS_TOTAL_COUNT)
            .addGroup(HealthConstants.StepCount.DEVICE_UUID, ALIAS_DEVICE_UUID)
            //.setLocalTimeRange(HealthConstants.StepCount.START_TIME, HealthConstants.StepCount.TIME_OFFSET,
            //        startTime, startTime + ONE_DAY)
            //.setSort(ALIAS_TOTAL_COUNT, SortOrder.DESC)
            .build();
	/*
    try {
        mResolver.aggregate(request).setResultListener(result -> {
            int totalCount = 0;
            String deviceUuid = null;

            try {
                Iterator<HealthData> iterator = result.iterator();
                if (iterator.hasNext()) {
                    HealthData data = iterator.next();
                    totalCount = data.getInt(ALIAS_TOTAL_COUNT);
                    deviceUuid = data.getString(ALIAS_DEVICE_UUID);
                }
            } finally {
                result.close();
            }

            if (mObserver != null) {
                mObserver.onChanged(totalCount);
            }

            if (deviceUuid != null) {
                readStepCountBinning(startTime, deviceUuid);
            }
        });
    } catch (Exception e) {
        Log.e(MainActivity.TAG, "Getting step count fails.", e);
    }*/
}

function getTodayStartUtcTime() {
    var today = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    console.log("Today : " + today.getTimeInMillis());

    today.set(Calendar.HOUR_OF_DAY, 0);
    today.set(Calendar.MINUTE, 0);
    today.set(Calendar.SECOND, 0);
    today.set(Calendar.MILLISECOND, 0);

    return today.getTimeInMillis();
}
 
module.exports = StepCountReader;