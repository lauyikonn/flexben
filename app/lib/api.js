/*********************
*** SETTING / API ***
**********************/
var API_DOMAIN = "https://valor.geonn.com.my";
var LIVE_API_DOMAIN = "https://flexben.my";
var DEV_API_DOMAIN = "https://valor.geonn.com.my";
API_DOMAIN = LIVE_API_DOMAIN;
var COMMON = require('common');
// APP authenticate user and key
var USER  = 'myworkwell';
var KEY   = '45lf17530472103d532l43dov207c8940p8';
var checkAppVersionUrl = API_DOMAIN+"/api/checkAppVersion_v2?user="+USER+"&key="+KEY;

/*********************
**** API FUNCTION*****
**********************/

// call API by post method
exports.callByPost = function(e, handler){
	var API = require('api');
	var deviceToken = Ti.App.Properties.getString('deviceToken');
	if(deviceToken != ""){ 
		var url = (e.fullurl)?e.url:API_DOMAIN+"/"+e.url+"?user="+USER+"&key="+KEY;
		console.log(url+" callByPost");
		console.log(e.params);
		
		var _result = contactServerByPostImage(url, e.params || {});  
		
		//var _result = contactServerByPost(url, e.params || {});   
		_result.onload = function(ex) {  
			if(e.skipJSON){
				_.isFunction(handler.onload) && handler.onload(this.responseText); 
				_.isFunction(handler.onfinish) && handler.onfinish(this.responseText); 
				return ;
			}
			try{
				JSON.parse(this.responseText);
			}
			catch(e){
				console.log(this.responseText);
				console.log('callbypost JSON exception');
				console.log(e);
				COMMON.createAlert("Whoops","There was a problem connecting with Server.", handler.onexception);
				_.isFunction(handler.onfinish) && handler.onfinish(this.responseText);
				return;
			}
			_.isFunction(handler.onload) && handler.onload(this.responseText); 
			_.isFunction(handler.onfinish) && handler.onfinish(this.responseText); 
		};
		
		_result.onerror = function(ex) {
			//-1001	The request timed out.
			console.log(ex.code);
			if(ex.code == "-1009"){		//The Internet connection appears to be offline.
				COMMON.createAlert("Error", ex.error, handler.onerror);
				return;
			}
			if(_.isNumber(e.retry_times)){
				console.log(e.retry_times);
				e.retry_times --;
				if(e.retry_times > 0){
					API.callByPost(e, handler);
				}else{
					console.log('onerror msg');
					console.log(ex);
					COMMON.createAlert("Error", ex.error, handler.onerror);
					_.isFunction(handler.onfinish) && handler.onfinish(this.responseText); 
				}
			}else{
				console.log('onerror msg without no');
				e.retry_times = 2;
				API.callByPost(e, handler);
			}
			
		};
	}
};

function contactServerByPostVideo(url,params) { 
	var client = Ti.Network.createHTTPClient({
		timeout : 50000
	});
	 
	//client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');  
	client.open("POST", url);
	client.onsendstream = function(e) {
	    console.log( Math.floor(e.progress * 100) + "%");
	};
	client.send(params); 
	return client;
};

exports.checkAppVersion = function(callback_download){
	var appVersion = Ti.App.version;
	console.log(appVersion);
	var url = checkAppVersionUrl + "&appVersion="+appVersion+"&appPlatform="+Titanium.Platform.osname;
	var client = Ti.Network.createHTTPClient({
		// function called when the response data is available
		onload : function(e) {
			var result = JSON.parse(this.responseText);

			if(result.status == "error"){
				callback_download && callback_download(result);
			}
		},
		// function called when an error occurs, including a timeout
		onerror : function(e) {
		},
		timeout : 60000  // in milliseconds
	});
	client.open("GET", url);
	client.setRequestHeader('Connection', "close");
	client.send();
};

// call API by post method
exports.callByPostImage = function(e, onload, onerror) { 
	var API = require('api');
	var client = Ti.Network.createHTTPClient({
		timeout : 5000
	});
	var url = eval(e.url);
	var _result = contactServerByPostImage(url, e.params || {});
	_result.onload = function(e) { 
		console.log('success');
		onload && onload(this.responseText); 
	};
	
	_result.onerror = function(ex) { 
		console.log("onerror");
		API.callByPostImage(e, onload);
		//onerror && onerror();
	};
};

// update user device token
exports.updateNotificationToken = function(e){
	
	var deviceToken = Ti.App.Properties.getString('deviceToken');
	if(deviceToken != ""){ 
		var records = {};
		records['version'] =  Ti.Platform.version;
		records['os'] =  Ti.Platform.osname;
		records['model'] =  Ti.Platform.model;
		records['macaddress'] =  Ti.Platform.macaddress;  
		records['token'] =  deviceToken;    
		var url = updateTokenUrl ;
		var _result = contactServerByPost(url,records);   
		_result.onload = function(e) {  
		};
		
		_result.onerror = function(e) { 
		};
	}
};

/*********************
 * Private function***
 *********************/

function contactServerByGet(url) { 
	var client = Ti.Network.createHTTPClient({
		timeout : 5000
	});
	client.open("GET", url);
	client.send(); 
	return client;
};

function contactServerByPost(url,records) { 
	var client = Ti.Network.createHTTPClient({
		timeout : 20000
	});
	client.open("POST", url);
	client.send(records);
	return client;
};

function contactServerByPostImage(url, records) { 
	var client = Ti.Network.createHTTPClient({
		timeout : 20000
	});
	client.open("POST", url);
	client.send(records); 
	return client;
};