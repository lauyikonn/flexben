exports.definition = {
	config: {
		columns: {
		    "id": "INTEGER PRIMARY KEY AUTOINCREMENT",
		    "step_count": "INTEGER",
		    "created" : "TEXT",
		    "u_id": "TEXT",
		},
		adapter: {
			type: "sql",
			collection_name: "steps"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
			addColumn : function( newFieldName, colSpec) {
				var collection = this;
				var db = Ti.Database.open(collection.config.adapter.db_name);
				if(Ti.Platform.osname != "android"){
                	db.file.setRemoteBackup(false);
                }
				var fieldExists = false;
				resultSet = db.execute('PRAGMA TABLE_INFO(' + collection.config.adapter.collection_name + ')');
				while (resultSet.isValidRow()) {
					if(resultSet.field(1)==newFieldName) {
						fieldExists = true;
					}
					resultSet.next();
				}
			 	if(!fieldExists) { 
					db.execute('ALTER TABLE ' + collection.config.adapter.collection_name + ' ADD COLUMN '+newFieldName + ' ' + colSpec);
				}
				db.close();
			},
			createStepsEntry: function(step){
			    var collection = this;
                var u_id = Ti.App.Properties.getString('u_id');
			    var isDateAlreadyPresent = false;
                var createSuccessful = false;
                var currentDateStepCounts = 0;
                var moment = require('alloy/moment');
                var todayDate = moment(new Date()).format("DD-MM-YYYY");
                var selectQuery = "SELECT step_count FROM " 
    + collection.config.adapter.collection_name + " WHERE created = '"+ todayDate+"' AND u_id = ?";
                try {
                      db = Ti.Database.open(collection.config.adapter.db_name);
                      var res = db.execute(selectQuery, u_id);
                       while (res.isValidRow()){ 
                           isDateAlreadyPresent = true;
                           currentDateStepCounts = res.fieldByName('step_count');
                           res.next();
                       }
                      db.close();
                  } catch (e) {
                    console.log(e);
                  }
                  try {
                      db = Ti.Database.open(collection.config.adapter.db_name);
                      if(isDateAlreadyPresent)
                      {
                        var sql_query = "UPDATE " + collection.config.adapter.collection_name + " SET step_count = ? where created = ? AND u_id = ?";
                        console.log(currentDateStepCounts + step+" updated");
                        var row = db.execute(sql_query, currentDateStepCounts + step, todayDate, u_id);
                        if(row == 1)
                        {
                          createSuccessful = true;
                        }
                        db.close();
                      }
                      else
                      {
                        var sql_query =  "INSERT OR REPLACE INTO "+collection.config.adapter.collection_name+" (step_count, u_id, created) VALUES (?,?,?)";
                        var row = db.execute(sql_query, 1, u_id, todayDate);
                        if(row!=-1)
                        {
                          createSuccessful = true;
                        }
                        db.close();
                      }
                  
                } catch (e) {
                  console.log(e);
                }
                return createSuccessful;
			},
			getData: function(){
				var collection = this;
                var u_id = Ti.App.Properties.getString('u_id'); 
                db = Ti.Database.open(collection.config.adapter.db_name);
                var sql = "SELECT * FROM " + collection.config.adapter.collection_name+" WHERE u_id = ?";
                 
                var res = db.execute(sql, u_id);
                var listArr = []; 
                var count = 0;
                 
                while (res.isValidRow()){ 
					listArr[count] = {
					 	id: res.fieldByName('id'),
					 	u_id: res.fieldByName("u_id"),
					    step_count: res.fieldByName('step_count'),
					    created :  res.fieldByName('created')
					}; 
					res.next();
					count++;
				} 
				res.close();
                db.close();
                collection.trigger('sync');
               
                return listArr;
			},
			getDataGroupByMonth: function(e){
				var collection = this;
				var type = e.type;
				var select_year = e.select_year+"";
                var u_id = Ti.App.Properties.getString('u_id'); 
                
                db = Ti.Database.open(collection.config.adapter.db_name);
                var sql = "SELECT *, strftime('%m', date) as day FROM " + collection.config.adapter.collection_name+" WHERE u_id = ? AND `type` = ? AND strftime('%Y', date) = ? group by strftime('%Y-%m', date)";
                 
                var library = Alloy.Collections.instance("health");
                library.fetch({query: {
					statement: sql,
						params: [u_id, type, select_year]
					}
				}); 
                 
                var res = db.execute(sql, u_id, type, select_year);
                var listArr = []; 
                var count = 0;
                 
                while (res.isValidRow()){ 
					listArr[count] = {
					 	id: res.fieldByName('id'),
					 	u_id: res.fieldByName("u_id"),
					    date: res.fieldByName('date'),
					    day: res.fieldByName('day'),
					    time: res.fieldByName('time'),
					    type: res.fieldByName('type'),
					    field1: res.fieldByName('field1'),
					    field2: res.fieldByName('field2'),
					    field3: res.fieldByName('field3'),
					    field4: res.fieldByName('field4'),
					    remark: res.fieldByName('remark'),
					    amount: res.fieldByName('amount'),
					    created :  res.fieldByName('created')
					}; 
					res.next();
					count++;
				} 
				res.close();
                db.close();
                collection.trigger('sync');
               
                return listArr;
			},
		});

		return Collection;
	}
};