var args = arguments[0] || {};
var callback;
$.button_title.text = args.hintText; 
var source;
$.init = function (arg) {
	callback = arg.callback;
	source = arg.source;
	console.log(source.vitality_type);
};

function showCamera(){
	Titanium.Media.showCamera({
        success:function(event) {
           //image_preview(event);
           callback(event, source);
        },
        cancel:function(){
            //do somehting if user cancels operation
        },
        error:function(error) {
            //error happend, create alert
            var a = Titanium.UI.createAlertDialog({title:'Camera'});
            //set message
            if (error.code == Titanium.Media.NO_CAMERA){
                a.setMessage('Device does not have camera');
            }else{
                a.setMessage('Unexpected error: ' + error.code);
                }

                // show alert
            a.show();
        },
        allowImageEditing:false,
        mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
        saveToPhotoGallery:true
    });
}

function popCamera(e){

	if(!filepermittion()) return;

	var dialog = Titanium.UI.createOptionDialog({
	    title: 'Choose an image source...',
	    options: ['Camera','Photo Gallery', 'Cancel'],
	    cancel:2 //index of cancel button
	});
	var pWidth = Ti.Platform.displayCaps.platformWidth;
    var pHeight = Ti.Platform.displayCaps.platformHeight;

	dialog.addEventListener('click', function(ex) {

	    if(ex.index == 0) { //if first option was selected
	        //then we are getting image from camera]
	        if(Ti.Media.hasCameraPermissions()){
		        showCamera();
	        }else{
		        Ti.Media.requestCameraPermissions(function(request_e){
		        	if(request_e.success){
		        		showCamera();
		        	}
		        	else{
		        		alert("You denied permission.");
		        	}
		        });
	        }
	    }else if(ex.index == 1){

			Titanium.Media.openPhotoGallery({

	            success:function(event) {
	            	//saveLocal({message: "", format:"photo", filedata: event});
	           		//image_preview(event);
	           		console.log("ready to callback and pass parent");
	           		console.log(args.vitality_type);
	           		source['vitality_type'] = args.vitality_type;
	           		console.log(source.vitality_type);
	           		callback(event, source);
				},
				cancel:function() {
					// called when user cancels taking a picture
				},
				error:function(error) {
					// called when there's an error
					var a = Titanium.UI.createAlertDialog({title:'Camera'});
					if (error.code == Titanium.Media.NO_CAMERA) {
						a.setMessage('Please run this test on device');
					} else {
						a.setMessage('Unexpected error: ' + error.code);
					}
					a.show();
				},
			    // allowEditing and mediaTypes are iOS-only settings
				allowEditing: false,
	            mediaTypes : [Ti.Media.MEDIA_TYPE_PHOTO],
	        });
	    }
	});

	//show dialog
	dialog.show();
}

function filepermittion()
{
	if(OS_ANDROID)
	{
		if(Ti.Filesystem.hasStoragePermissions()) return true;
		else{
			 Ti.Filesystem.requestStoragePermissions(function(e){
			    if(e.success){
			    	return true;
			    }
			    else{
			        alert("You have denied the permission.");
			        return false;
			   	}
			 });
		}
	}else{
		if(Ti.Media.hasPhotoGalleryPermissions()) return true;
		else{
			 Ti.Media.requestPhotoGalleryPermissions(function(e){
			    if(e.success){
			    	return true;
			    }
			    else{
			        alert("You have denied the permission.");
			        return false;
			   	}
			 });
		}
	}
}

function image_preview(event){
	if(typeof $.container.children[1] != "undefined"){
		$.container.children[1].image = event.media;
	}else{
		$.container.add($.UI.create("ImageView", {image: event.media, width: 120, classes:['hsize']}));
	}
}
