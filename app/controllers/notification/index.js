var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
var COMMON = require('common');
function init(){
	$.win.add(loading.getView());
	refresh({});
}
init();

function refresh(e){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: "api/getNotificationList", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_list(arr){
	$.listing.removeAllChildren();
	console.log(arr.length+" arr.length");
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		if(arr[i].format == "image"){
		  var image = $.UI.create("ImageView", {classes:['wfill','hsize'], image: arr[i].url});
		  view_container.add(image);
		  image.addEventListener("click", function(e){
		      console.log(e.source.image);
		      if(OS_ANDROID){
		          var win = Alloy.createController("webview", {url: e.source.image, title: ""}).getView();
		          win.open();
		      }else{
		          COMMON.lightbox({img_path: e.source.image}, $.win);
		      }
		      
		  });
		}else if(arr[i].format == "web"){
		  var image = $.UI.create("ImageView", {height: 40, top:10, width: 40, url: arr[i].url, title: arr[i].subject, image: "/images/icons/pdf.png"});
		  view_container.add(image);
		  image.addEventListener("click", function(e){
		      
	          if(OS_IOS){
	          	var win = Alloy.createController("webview", {url:e.source.url, title: e.source.title}).getView();
				Alloy.Globals.drawer.centerWindow.openWindow(win);
			  }else{
			  	Ti.Platform.openURL(e.source.url);
				//win.open();
			  }
		  });
		}
		var label_message = $.UI.create("Label", {classes:['wfill','hsize','padding'], bottom:5, text: arr[i].message});
        view_container.add(label_message);
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
		//var text_address = arr[i].add1+" "+arr[i].add2+" "+arr[i].city+" "+arr[i].state;
		var label_update = $.UI.create("Label", {classes:['wfill','hsize','padding','bold', 'h6'], top:0, text: arr[i].updated});
		var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
		//var button_map = $.UI.create("Button", {width: "40%", left: 10, bottom:10,  title: "GET DIRECTION", text_address:text_address, coord: arr[i].latitude+", "+arr[i].longitude, tel: arr[i].tel});
		
		//view_container.add(view_hr);
		
		view_container.add(label_update);
		//view_action.add(button_map);
		view_container.add(view_action);
		
		$.listing.add(view_container);
		
	};
}

function closeWindow(e){
	$.win.close();
}

