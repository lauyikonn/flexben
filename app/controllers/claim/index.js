var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh();
}
init();

function refresh(){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	var params = {u_id: u_id, isEmployee:1};
	API.callByPost({url: "api/getStaffEntitlement_v2", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(typeof result.data != "undefined" && result.data.length > 0){
				render_entitlement_new(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_entitlement_new(arr){
	$.ent_list.removeAllChildren();
	for (var i=0; i < arr.length; i++) {
		var view_master= $.UI.create("View", {classes:['wfill','hsize','padding','grey']});
		var view_container = $.UI.create("View", {classes:['wfill','hsize','padding','horz'], bottom:0});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize', 'h4', 'bold'], text: arr[i].name, bottom:5});
		view_master.add(view_container);
		view_container.add(label_name);
		
		for (var j=0; j < arr[i].entitlement.length; j++) {
			var entitlement = arr[i].entitlement[j];
			if(typeof entitlement.balance_pa_percent != "undefined"){
				console.log("a1");
				var parcent = entitlement.balance_pa_percent;
				console.log(entitlement.balance_pa_percent);
				var webview_chart = $.UI.create("WebView", {left:0, top:0, bottom:0, url:"/html/donut_chart.html", touchEnabled: false, backgroundColor: "#42434b", width: 100, height: 100});
				view_container.add(webview_chart);
				webview_chart.addEventListener("load", function(e){
					console.log("a2");
					console.log(parcent);
					e.source.evalJS("run('"+parcent+"');");
				});
				
			}
			var view_container2 = $.UI.create("View", {classes:['hsize','horz'],width: "auto", top:0, left:10});
			view_container.add(view_container2);
			console.log(entitlement);
			
			var view_div= $.UI.create("View", {top:5, righ: 10, bottom: 10, height: 1, classes:['wfill','bg_light_blue']});
			view_container.add(view_div);
			
			for(var k = 0; k < entitlement.additional_info.length; k++){
				view_container2.add(insert_addition_info(entitlement.additional_info[k]));
			};
		}
		$.ent_list.add(view_master);
	}
}

function insert_addition_info(e){
	var view_container = $.UI.create("View", {classes:['wsize','hsize','vert'], right:20});
	var label_title = $.UI.create("Label", {classes:['wsize','hsize', 'h5', 'bold'], left:0, text: e.title});
	var label_value = $.UI.create("Label", {classes:['wsize','hsize', 'h5'], left:0, text: e.value, bottom:5});
	view_container.add(label_title);
	view_container.add(label_value);
	
	return view_container;	
}

function render_entitlement(arr){
	return;
    $.ent_list.removeAllChildren();
	for (var i=0; i < arr.length; i++) {
		var view_div = $.UI.create("View", {classes:['wfill', 'bg_blue'], top:10, height: 10});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize', 'h5', 'bold', 'blue', 'padding'], text: arr[i].name, bottom:0});
		
		if(i>0){
			$.ent_list.add(view_div);
		}
		$.ent_list.add(label_name);
		if(arr[i].entitlement.length <= 0){
			var text_no_entitlement = $.UI.create("Label", {classes:['wfill','hsize','h6','padding'], textAlign: "center", text: "No entitlement available"});
			$.ent_list.add(text_no_entitlement);
		}
		for (var j=0; j < arr[i].entitlement.length; j++) {
			var entitlement = arr[i].entitlement[j];
			
			var view_entitlement = $.UI.create("View", {classes:['wfill', 'hsize', 'vert', 'padding']});
			var view_ent_title = $.UI.create("View", {classes:['wfill', 'hsize'], bottom: 5});
			var label_category = $.UI.create("Label", {classes:['wsize','hsize','h5','bold','green'], bottom:5, left:0, text: entitlement.category});   
			
			//view_ent_title.add(label_category);
			//view_ent_title.add(label_balance);
			view_entitlement.add(label_category);
			console.log((entitlement.limit_pa - entitlement.balance_pa)+" "+entitlement.limit_pa);
			var percentage_usage = (entitlement.limit_pa == -1)?100:Math.round(( entitlement.balance_pa / entitlement.limit_pa)*100);
			var percentage_text = (entitlement.limit_pa == -1)?"UNLIMITED":percentage_usage+"% Balance";
			var text_color = (entitlement.limit_pa == -1 || percentage_usage > 27)?"black":"black";
			var text_bg = (entitlement.limit_pa == -1 || percentage_usage > 10)?"bg_green":"bg_red";
			if(text_bg == "bg_green"){
				text_bg = (percentage_usage < 75)?"bg_yellow":"bg_green";
			}
			text_bg = "bg_orange";
			var view_ent_bar = $.UI.create("View", {classes:['wfill', 'hsize'], backgroundColor: "#ffffff"});
			var view_ent_barcolor = $.UI.create("View", {classes:[text_bg], left:0, height: 30, width: percentage_usage+"%"});
			var label_percentage = $.UI.create("Label", {classes:['wsize','hsize','bold', text_color], text: percentage_text, left: 10});
			
			view_ent_bar.add(view_ent_barcolor);
			view_ent_bar.add(label_percentage);
			view_entitlement.add(view_ent_bar);
			
			var view_ent_period = $.UI.create("View", {classes:['wfill', 'hsize']});
			var label_period = $.UI.create("Label", {classes:['hsize','h6'], width: "49%", text: "Period: \nFrom "+entitlement.start_date+" till "+entitlement.end_date, left: 0, top:5});
			var text_balance = (entitlement.limit_pa == -1)?"UNLIMITED":"Balance: \n"+entitlement.balance_pa+"/"+entitlement.limit_pa;
			var label_balance = $.UI.create("Label", {classes:['hsize','h6'], width: "49%", top:5, right:0, text: text_balance});
			
			view_ent_period.add(label_period);
			view_ent_period.add(label_balance);
			view_entitlement.add(view_ent_period);
			
			$.ent_list.add(view_entitlement);
		};
	};
}

//refresh private function end

function navTo(e){
	var win = Alloy.createController(e.source.target, e.source.param || {}).getView();
	var target = e.source.target;
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();	
	}
}

function closeWindow(e){
	$.win.close();
}


