var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh();
}
init();

function refresh(){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	console.log(u_id+" uid");
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	API.callByPost({url: "api/getClaimList", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(typeof result.data != "undefined" && result.data.length > 0){
				render_listing(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_listing(arr){
	console.log(arr.length+" arr.length");
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		var view_name_arrow = $.UI.create("View", {classes:['wfill','hsize']});
		var label_patient_name = $.UI.create("Label", {classes:['wfill','hsize','padding','h4'], right: 50, text: arr[i].patient_name});
		var image_arrow = $.UI.create("ImageView", {image:"/images/icons/next.png", width: 20, height: 20, right:10, top: 10, target: "claim/detail", record: arr[i]});
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
		var text_clinic_name = arr[i].clinic_name+"\n"+arr[i].claim_type+"\n"+arr[i].visit_date+"\nStatus: "+arr[i].status;
		var label_sub1 = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: text_clinic_name});
		var label_sub2 = $.UI.create("Label", {classes:['wfill','hsize','padding', 'bold'], top:0, text: "Amount: "+arr[i].total_amount});
		var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
		var button_detail = $.UI.create("Button", {width: "40%", left: 10, bottom:10, title: "AMOUNT DETAIL", target: "claim/detail", record: arr[i]});
		
		view_name_arrow.add(label_patient_name);
		view_name_arrow.add(image_arrow);
		view_container.add(view_name_arrow);
		view_container.add(view_hr);
		view_container.add(label_sub1);
		view_container.add(label_sub2);
		view_action.add(button_detail);
		//view_container.add(view_action);
		
		$.listing.add(view_container);
		image_arrow.addEventListener("click", navTo);
	};
}

function navTo(e){
	var win = Alloy.createController(e.source.target, {claim_id: e.source.record.id, type: e.source.record.type} || {}).getView();
	var target = e.source.target;
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();	
	}
}

function tel_call(e){
	var tel = e.source.tel.replace(/[+]/g, "");
	Ti.Platform.openURL('tel:+'+tel);
	Ti.Platform.openURL("tel:"+tel);
}

function closeWindow(e){
	$.win.close();
}


