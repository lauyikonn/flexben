var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh();
}
init();

function refresh(){
	loading.start();
	console.log(args.claim_id+" args.claim_id "+args.type);
	var params = {claim_id: args.claim_id, type: args.type};
	API.callByPost({url: "api/getClaimDetails", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			if(typeof result.data != "undefined"){
				console.log(typeof result.data);
				if(typeof result.data != "undefined"){
					render_listing(result.data);
				}
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_listing(arr){
	console.log(arr);
	console.log(typeof arr);
	var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
	for (var key in arr) {
    	if (arr.hasOwnProperty(key) && arr[key] != null) {
    		arr[key] = arr[key].replace(/\\n/g, "\n");
			var label_amount_key = $.UI.create("Label", {classes:['wfill','hsize','padding','h4'], bottom:0, text: key});
			var label_amount_value = $.UI.create("Label", {classes:['wfill','hsize','padding','h5'], top:0, text: arr[key]});
			var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
			view_container.add(label_amount_key);
			view_container.add(label_amount_value);
			view_container.add(view_hr);
			console.log(key, arr[key]);
		}
   	};
   	$.listing.add(view_container);
}

function url_scheme(e){
	Ti.Platform.openURL((OS_IOS)?"maps":"http"+"://maps.google.com/maps?q="+e.source.text_address+"&hl=en&daddr="+e.source.coord+"&spn="+e.source.coord+"&daddr="+e.source.coord);
}

function closeWindow(e){
	$.win.close();
}


