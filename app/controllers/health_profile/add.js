var args = arguments[0] || {};
//var fields = args.fields;
var moment = require('alloy/moment');
var API = require('api');
init();

function init(){
	var model = Alloy.createCollection("health");
	render_field_type();
	$.date.value = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
	//$.date_text.text = moment(new Date()).format("ddd, MMM DD, YYYY, hh:mm A");
	//refresh();
}

function render_field_type(){
	var fields = args.fields;//.reverse();
	var fields_arr = [];
	for (var i=0; i < fields.length; i++) {
		var view_container = $.UI.create("View", {classes:['padding','wsize','hsize','vert'], top: 0, bottom:0});
		var view_left = $.UI.create("View", {classes:['wfill','hsize']});
		var label_title = $.UI.create("Label", {classes:['wsize','hsize','h5'], left:0, text: fields[i].name});
		var view_hr = $.UI.create("View", {classes:['hr'], backgroundColor: "#ddd"});
		var default_value = (typeof fields[i].default_value != "undefined")?fields[i].default_value:"";
		view_left.add(label_title);
		if(fields[i].tool == "picker"){
            var view_picker = $.UI.create("View", {classes: ['combo_box'], required:1, top: 0, default_value: default_value, id: fields[i].name, record:fields[i]});
            var label_picker = $.UI.create("Label", {classes:['wfill','hsize','padding', "h5"], touchEnabled: false, text: fields[i].name});
            view_picker.add(label_picker);
            view_picker.addEventListener("click", openPicker);
			$.container.add(view_picker);
			fields_arr.push(view_picker);
		}else if(fields[i].tool == "textfield"){
			var textfield_value = $.UI.create("TextField", {classes:['padding'], hintText:fields[i].name, top:0, value: default_value, id: fields[i].name, record:fields[i]});
			$.container.add(textfield_value);
			fields_arr.push(textfield_value);
		}else if(fields[i].tool == "remark"){
			var textfield_value = $.UI.create("TextField", {classes:['padding'], hintText:fields[i].name, top:0, value: default_value, id: fields[i].name, record:fields[i] });
			$.container.add(textfield_value);
			fields_arr.push(textfield_value);
		}
		view_container.add(view_left);
	};
}

function popout(arr, e){
    arr.push("Cancel");
    var dialog = Ti.UI.createOptionDialog({
        cancel: arr.length - 1,
        options: arr,
        //selectedIndex: e.source.value || 0,
        //title: e.source.children[0].text
    });
        
    dialog.show(); 
    dialog.addEventListener("click", function(ex){
        console.log(ex.index+" "+ex.cancel);
        if(!ex.cancel){
            e.source.children[0].text = arr[ex.index];
            e.source.value = arr[ex.index];
            e.source.record.default_value = arr[ex.index];
        }
    });
}

function openPicker(e){
	var field = e.source.record;
	if(field.type == "number"){
		var min = (typeof field.min_range != "undefined")?field.min_range:0;
		var max = (typeof field.max_range != "undefined")?field.max_range:0;
		var options = _.range(min, max);
	}else if(field.type == "string"){
		var options = field.options;
	}
	if(OS_ANDROID){
	    popout(options, e);
	    return;
	}
	var view_mask = $.UI.create("View", {classes:['wfill','hfill'], backgroundColor: "#ffffff"});
	var picker = $.UI.create("Picker", {bubbleParent: false});
	var button_save  = $.UI.create("Button", {classes:['small_button', 'rounded'], left:20, right:20, top:20, width: Ti.UI.FILL, title: "Select"});
	
	for (var i=0; i < options.length; i++) {
		var label = $.UI.create("Label", {classes:['h6'], textAlign: "center", text: options[i]});
		var picker_row = $.UI.create("PickerRow", {value: options[i]});
		picker_row.add(label);
		picker.add(picker_row);
		
		if(e.source.children[0].text != "" && (e.source.children[0].text == options[i] || e.source.default_value == options[i])){
			picker.setSelectedRow(0, i, false);
		}
	};
	
	view_mask.add(picker);
	view_mask.add(button_save);
	$.win.add(view_mask);
	picker.addEventListener("change", function(ex){
		//alert(picker.getSelectedRow(0).title);
	});
	button_save.addEventListener("click", function(){
		console.log(picker.getSelectedRow(0).value);
		if(picker.getSelectedRow(0).value == "Cancel"){
			return;
		}
		e.source.children[0].text = picker.getSelectedRow(0).value;
		e.source.value = picker.getSelectedRow(0).value;
		e.source.record.default_value = picker.getSelectedRow(0).value;
		console.log(e.source.children[0].text);
		$.win.remove(view_mask); 
	});
}

function showDatePicker(e){ 

	if(OS_ANDROID){ 
		var datePicker = Ti.UI.createPicker({
			  type: Ti.UI.PICKER_TYPE_DATE,
			  id: "datePicker",
			  value: e.source.default_value || new Date(),
			  visible: false
		});
		datePicker.showDatePickerDialog({
			value: new Date(),
			callback: function(ex) {
			if (ex.cancel) { 
				} else {
				    console.log(ex);
				    var value = ex.value;
					e.source.children[0].text = moment(value).format("ddd, MMM DD, YYYY, hh:mm A");
                    e.source.value = moment(value).format("YYYY-MM-DD HH:mm:ss");
                    e.source.default_value = value;
				}
			}
		});
	}else{
		
		var view_mask = $.UI.create("View", {classes:['wfill','hfill'], backgroundColor: "#ffffff"});
		var picker_date = $.UI.create("Picker", {type: Ti.UI.PICKER_TYPE_DATE_AND_TIME, value:e.source.default_value || new Date(), bubbleParent: false});
		var button_save  = $.UI.create("Button", {classes:['small_button', 'rounded'], left:20, right:20, top:20, width: Ti.UI.FILL, title: "Select"});
		view_mask.add(picker_date);
		view_mask.add(button_save);
		$.win.add(view_mask);
		
		button_save.addEventListener("click", function(ex){
		    var value = picker_date.value;
		    e.source.children[0].text = moment(value).format("ddd, MMM DD, YYYY, hh:mm A");
            e.source.value = moment(value).format("YYYY-MM-DD HH:mm:ss");
		    // e.source.children[0].text = picker_date.value;
            // e.source.value = picker_date.value;
            e.source.default_value = value;
            //var value = (OS_IOS)?ex.source.value:ex.value;
            //e.source.children[0].text = moment(value).format("ddd, MMM DD, YYYY, hh:mm A");
            // e.source.value = moment(value).format("YYYY-MM-DD HH:mm:ss");
            //e.source.default_value = value;
            console.log(value);
            $.win.remove(view_mask); 
        });
	}
}

function SaveRecord(){
	var u_id = Ti.App.Properties.getString('u_id') || 0;
	
	var params = {
			u_id: u_id,
		    type: args.type,
		    created : moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
	};
	var all_field = $.container.getChildren();
	var error_message = "";
	for (var i=0; i < all_field.length; i++) {
		if(all_field[i].id != "Remark" && i > 0){
            var fieldname = "field"+(i);
            params[fieldname] = all_field[i].value;
            console.log(all_field[i].value+" check here");
            if(all_field[i].value == "" || typeof all_field[i].value == "undefined"){
                error_message += all_field[i].id+" cannot be empty\n";
            }
        }else{
            params[all_field[i].id] = all_field[i].value;
        }
	};
	if(error_message != ""){
	    alert(error_message);
	    return;
	}
	var model = Alloy.createCollection("health");
	model.saveArray([params]);
	Ti.App.fireEvent("webview:graph_loaded");
	$.win.close();
	API.callByPost({url: "api/syncHealthData", params:params}, function(responseText)	{
		var res = JSON.parse(responseText);
	});
}

function closeWindow(e){
    $.win.close();
} 

  
$.win.addEventListener("close", function(e){
	Ti.App.fireEvent("myHealth:render_menu");
});
