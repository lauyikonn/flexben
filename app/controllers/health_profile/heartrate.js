(function (container) {
	var mCamera = null;
  	var MyCameraPreview = null;
  	var bitmap = null;
  	var pixels = null;
  	var FrameData = null;
  	var imageFormat;
  	var PreviewSizeWidth;
  	var PreviewSizeHeight;
  	var bProcessing = false;
  	var PreviewCallback = require('android.hardware.Camera.PreviewCallback');
  	var SurfaceHolder_Callback = require('android.view.SurfaceHolder.Callback');
  	var Handler = require('android.hardware.Camera.PreviewCallback');
  	var Looper = require('android.os.Looper');
  	var Runnable = require('java.lang.Runnable');
  	var camView = require('android.view.SurfaceView');
  	var mHandler = new Handler(Looper.getMainLooper());
  	
    
    PreviewCallback.onPreviewFrame = function(arg0, arg1){
    	if (imageFormat == ImageFormat.NV21)
	    {
	      //We only accept the NV21(YUV420) format.
	      if ( !bProcessing )
	      {
	        FrameData = arg0;
	        mHandler.post(DoImageProcessing);
	      }
	    }
    }
    
    SurfaceHolder_Callback.surfaceChanged = function(arg0, arg1, arg2, arg3){
	    var parameters;
	    parameters = mCamera.getParameters();
	    // Set the camera preview size
	    parameters.setPreviewSize(PreviewSizeWidth, PreviewSizeHeight);
	   
	    imageFormat = parameters.getPreviewFormat();
	   
	    mCamera.setParameters(parameters);
	   
	    mCamera.startPreview();
  	}
  	
  	SurfaceHolder_Callback.surfaceCreated = function(arg0) 
  	{
	    mCamera = Camera.open();
	    try
	    {
	      mCamera.setPreviewDisplay(arg0);
	      mCamera.setPreviewCallback(this);
	    } 
	    catch (e)
	    {
	      mCamera.release();
	      mCamera = null;
	    }
  	}
  	
  	SurfaceHolder_Callback.surfaceDestroyed = function(arg0) 
  	{
     	mCamera.setPreviewCallback(null);
  		mCamera.stopPreview();
  		mCamera.release();
  		mCamera = null;
  	}	
  	
  	function CameraPreview(PreviewlayoutWidth, PreviewlayoutHeight, CameraPreview)
  	{
	    PreviewSizeWidth = PreviewlayoutWidth;
	    PreviewSizeHeight = PreviewlayoutHeight;
	    MyCameraPreview = CameraPreview;
	    bitmap = Bitmap.createBitmap(PreviewSizeWidth, PreviewSizeHeight, Bitmap.Config.ARGB_8888);
	    pixels = PreviewSizeWidth * PreviewSizeHeight;
  	}

    var camHolder = camView.getHolder();
    camPreview = new CameraPreview(PreviewSizeWidth, PreviewSizeHeight, MyCameraPreview);
         
    camHolder.addCallback(camPreview);
    camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
         
    $.alert_container.add(camView);
    $.alert_container.add(MyCameraPreview); 
})($.alert_container);