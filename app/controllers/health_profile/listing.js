var args = arguments[0] || {};
var perior_type = "day";
var moment = require('alloy/moment');
var model = Alloy.createCollection("health");
var select_month = moment(new Date()).format("YYYY-MM");
var select_year = moment(new Date()).format("YYYY");
var field_graph = [];
init();

function init(){
	$.win.title = args.title;
	for (var i=0; i < args.fields.length; i++) {
        if(args.fields[i].graph_display){
            field_graph.push(args.fields[i]);
        }
    };
	refresh({});
}

function refresh(e){
	var data = model.getAllData({date: select_year, type: args.type, select_year: select_year});
	render_tableview(data);
}

function render_tableview(data){
	var arr = [];
	for (var i=0; i < data.length; i++) {
		var transformData = transformFunction(data[i]);
		var row = $.UI.create("TableViewRow");
		var view_container = $.UI.create("View", {classes:['wfill','hsize','padding','vert']});
		var label_title = $.UI.create("Label", {classes:['wfill','hsize','h5'], text: transformData.main_title});
		var label_subtitle = $.UI.create("Label", {classes:['wfill','hsize','h6'], text: transformData.sub_title});
		view_container.add(label_title);
		view_container.add(label_subtitle);
		row.add(view_container);
		arr.push(row);
	};
	console.log("what data inside");
	$.tbl.setData(arr);
}

function closeWindow(e){
    $.win.close();
}

function transformFunction(transform){

	var main_title = "";
	if(args.title == "BMI"){
		main_title = ((transform['field1'] / transform['field2'] / transform['field2']) * 10000).toFixed(2);
	}else{
		for (var k=0; k < field_graph.length; k++) {
			main_title += (k == 0)?transform['field'+(k+1)]:"/"+transform['field'+(k+1)];
		}
	}
	main_title += " "+args.measurement;
    transform.main_title = main_title;
    transform.sub_title = moment(transform.date).format("DD-MM-YYYY hh:mm A");
    return transform;
}

$.win.addEventListener("close", function(e){
});
