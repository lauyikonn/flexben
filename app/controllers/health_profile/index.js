var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
var menus = [
    {title: "STEPS", type:"10", icon: "/images/icons/cardiogram.png", disabled_add: true, measurement: "steps", color: "#CE1D1C", fields:[
        {name: "Steps", type: "number", tool: "picker", max_range: 200, min_range: 10, default_value: 100, graph_display: true},
    ]},
    {title: "BLOOD PRESSURE", type: "2", icon: "/images/icons/blood-pressure.png", measurement: "mmHg", color: "#CE1D1C", fields:[
        {name: "Systolic", type: "number", tool: "picker", max_range: 200, min_range: 10, default_value: 120, graph_display: true},
        {name: "Diastolic", type: "number", tool: "picker", max_range: 200, min_range: 10,  default_value: 100, graph_display: true},
        {name: "Remark", type: "string", tool: "remark", graph_display: false}
    ]},
    {title: "BLOOD GLUCOSE", type: "8", icon: "/images/icons/blood-drop.png", measurement: "mmol/L", color: "#CE1D1C", fields:[
        {name: "Blood Glucose", type: "number", tool: "picker", max_range: 15, min_range: 1, default_value: 5, graph_display: true},
        {name: "Remark", type: "string", tool: "remark", graph_display: false}
    ]},
    {title: "HEART RATE", type:"3", icon: "/images/icons/cardiogram.png", measurement: "bmp", color: "#CE1D1C", fields:[
        {name: "Pulse Rate", type: "number", tool: "picker", max_range: 200, min_range: 10, default_value: 100, graph_display: true},
    ]},
    /*{title: "WEIGHT", type:"6", icon: "/images/icons/scale.png", measurement: "KG", color: "#CE1D1C", fields:[
        {name: "Weight", type: "number", tool: "picker", max_range: 300, min_range: 0, default_value: 100, graph_display: true},
        //{name: "Body Fat %", type: "number", tool: "picker", max_range: 100, min_range: 0, default_value: 15, graph_display: true},
        {name: "Remark", type: "string", tool: "remark", graph_display: false}
    ]},*/
     {title: "BMI", type:"8", icon: "/images/icons/scale.png", measurement: "BMI", color: "#CE1D1C", fields:[
        {name: "Weight (KG)", type: "number", tool: "picker", max_range: 300, min_range: 0, default_value: 100, graph_display: true},
        {name: "Height (cm) ", type: "number", tool: "picker", max_range: 250, min_range: 0, default_value: 150, graph_display: true},
        {name: "Remark", type: "string", tool: "remark", graph_display: false}
    ]},
    {title: "CHOLESTROL", type:"7", icon: "/images/icons/blood-analysis.png", measurement: "mg/dL", color: "#CE1D1C", fields:[
        {name: "HDL", type: "number", tool: "picker", max_range: 200, min_range: 0, default_value: 100, graph_display: true},
        {name: "LDL", type: "number", tool: "picker", max_range: 300, min_range: 0, default_value: 130, graph_display: true},

        {name: "Remark", type: "string", tool: "remark", graph_display: false}
    ]},
];
init();

function init(){
	if(OS_IOS){
    	var step = require("step_service");
    	step.recentSteps();
    }else{
    	menus.shift();
    }
    $.win.add(loading.getView());
    render_menu();
    //refresh();
}

function render_menu(){
    var model = Alloy.createCollection("health");
    var latest = model.getLatestByType();
    console.log(latest);
    var pw = Ti.Platform.displayCaps.platformWidth;
    var ldf = Ti.Platform.displayCaps.logicalDensityFactor;
    var pwidth = (OS_IOS)?pw:parseInt(pw / (ldf || 1), 10);

    var cell_width =  Math.floor((pwidth - 70) / 2 );
    var odd_counter = 0;
    var arr_tbl = [];
    for (var i=0; i < menus.length; i++) {
        var row = $.UI.create("TableViewRow", {record: menus[i]});
        var tbl_view_container = $.UI.create("View", {classes:['wfill','hsize','padding'], touchEnabled: false});
        var tbl_label_title = $.UI.create("Label", {classes:['wfill','hsize','h5'], touchEnabled: false, left:40, text: menus[i].title});
        var tbl_image_icon = $.UI.create("ImageView", {width: 20, height:20, left: 10, tintColor: menus[i].color, image: menus[i].icon, touchEnabled: false});
        tbl_view_container.add(tbl_label_title);
        tbl_view_container.add(tbl_image_icon);
        row.add(tbl_view_container);
        arr_tbl.push(row);

        var found = _.where(latest, {type: menus[i].type});
        var top = Math.floor(i/2)*180+5;
        var left = (odd_counter % 2)?cell_width+10:5;
        var view_container = $.UI.create("View", {classes:['vert'], width: cell_width, height: 155, top:10, left: 10, backgroundColor: "#E89114", record: menus[i]});
        var label_title = $.UI.create("Label", {classes:['wfill','hsize','h6','white','padding'], textAlign: "center", top:10, text: menus[i].title, touchEnabled: false});
        var image_icon = $.UI.create("ImageView", {width: 50, height:50, top:10, bottom:10, image: menus[i].icon, touchEnabled: false});
        var main_title = "";
        if(found.length > 0){
        	console.log(found);
        	if(menus[i].title == "BMI"){
        		main_title = ((found[0]['field1'] / found[0]['field2'] / found[0]['field2']) * 10000).toFixed(2);
        	}else{
	            for (var j=0; j < menus[i].fields.length; j++) {
	                if(menus[i].fields[j].graph_display){
	                    main_title += (j == 0)?found[0]['field'+(j+1)]||0:"/"+found[0]['field'+(j+1)]||0;
	                }
	            };
           }
        }
        main_title = (main_title == "")?0:main_title;
        if(typeof $.menu.children[i] != "undefined"){
            $.menu.children[i].children[1].text = main_title;
        }else{
            var label_latest = $.UI.create("Label", {classes:['wfill','hsize','h3'], textAlign: "center", color: "#ffffff",  text: main_title, touchEnabled: false});
            var label_measurement = $.UI.create("Label", {classes:['wfill','hsize','h6'], textAlign: "center", color: menus[i].color,  text: menus[i].measurement, touchEnabled: false});
            //var button_record = $.UI.create("Button", {classes:['small_button','rounded','padding'], borderColor: menus[i].color, color: menus[i].color, width: Ti.UI.FILL, title: "Record", record: menus[i], bubbleParent: false});

            view_container.add(image_icon);
            view_container.add(label_latest);
            view_container.add(label_measurement);
            view_container.add(label_title);
            //view_container.add(button_record);
            $.menu.add(view_container);
            view_container.addEventListener('click', navToGraph);
            //button_record.addEventListener('click', navToAdd);
        }
        odd_counter++;
    };
    $.listing.setData(arr_tbl);
}

function navTo(e){
    var source = (typeof e.source != "undefined")?e.source:e;
    var param = (typeof source.param != "undefined")?source.param:{};
    console.log(e);
    console.log("check the e here");
    if(typeof source.to != "undefined"){
        _.extend(param, {to: source.to});
    }
    var win = Alloy.createController(source.target, param || {}).getView();
    var target = source.target;
    if(OS_IOS){
        Alloy.Globals.drawer.centerWindow.openWindow(win);
    }else{
        win.open();
    }
}

function navToGraph(e){
    console.log(e.source.record);
    navTo({target: "health_profile/graph", param: e.source.record});
    //nav.navigateWithArgs("myHealth/graph", e.source.record);
}

function navToAdd(e){
    navTo({target: "health_profile/add", param: e.source.record});
    //nav.navigateWithArgs("myHealth/add", e.source.record);
}

function refresh(){
    loading.start();
    var u_id = Ti.App.Properties.getString('u_id') || 0;
    var checker = Alloy.createCollection('updateChecker');
    var isUpdate = checker.getCheckerById("1", u_id);
    var last_updated ="";

    if(isUpdate != "" ){
        last_updated = isUpdate.updated;
    }

    API.callByPost({url: "getHealthDataByUser", params:{u_id: u_id, last_updated: last_updated}}, {
        onload: function(responseText){
            var model2 = Alloy.createCollection("health");
            var res2 = JSON.parse(responseText);
            var arr2 = res2.data || null;
            model2.saveArray(arr2);
            checker.updateModule(1,"getHealthDataByUser", res2.last_updated, u_id);
            render_graph();
        }, onfinish: function(){
            loading.finish();
        }
    });
}

function closeWindow(e){
    $.win.close();
}

Ti.App.addEventListener("myHealth:render_menu", render_menu);

$.win.addEventListener("close", function(e){
    Ti.App.removeEventListener("myHealth:render_menu", render_menu);
});
