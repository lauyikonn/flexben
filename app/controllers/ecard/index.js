var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	render_user_info();
	refresh();
}
init();

function refresh(){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	var params = {u_id: u_id, full:1};
	API.callByPost({url: "api/getEmployeeFamilyList", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(typeof result.data != "undefined" && result.data.length > 0){
				render_familyList(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_familyList(data){
	for (var i=1; i < data.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert'], bottom:10});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize','h6'], textAlign: "left", text: data[i].name});
		var label_nric = $.UI.create("Label", {classes:['wfill','hsize','h6'], textAlign: "left", text: "NRIC: "+data[i].nric});
		view_container.add(label_name);
		view_container.add(label_nric);
		$.dependant.add(view_container);
	};
}

function render_user_info(){
	$.name.text = "Employee: "+Ti.App.Properties.getString("name");
	$.company_logo.image =  Ti.App.Properties.getString("company_logo");
	$.company_name.text = "Company: "+Ti.App.Properties.getString("company_name");
	$.enquiries_email.text =  "Email: "+(Ti.App.Properties.getString("company_email") || Ti.App.Properties.getString("flexben_email"));
	$.enquiries_contact.text =  "Tel: "+(Ti.App.Properties.getString("company_contact") || Ti.App.Properties.getString("flexben_contact"));
	var master = "NRIC: "+Ti.App.Properties.getString("master") || "";
	$.empno.text = "Staff No: "+Ti.App.Properties.getString("empno") || "";
	console.log(Ti.App.Properties.getString("department"));
	console.log('here');
	$.other.text = (Ti.App.Properties.getString("department") || "" != "")?"Department: "+Ti.App.Properties.getString("department"):"";
	//$.location.text = "Location: "+Ti.App.Properties.getString("location") || "";
	$.master.text = master;// master.replace(/(.{4})/g, '$1  ');
}

function openURL(e){
	var win = Alloy.createController("webview", {url: e.source.target}).getView();
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

function closeWindow(e){
	$.win.close();
}
