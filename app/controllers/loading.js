var args = arguments[0] || {};
$.loadingBar.hide();
$.loadingpage.hide();
$.activityIndicator.hide();
/**
 * function to start the loading animation
 */
$.start = function() {
	$.loadingpage.show();
	$.loadingBar.show();
	$.activityIndicator.show();
};

/*
 * exposed function to finish the loading animation. Animates the rocket off the screen.
 */
$.finish = function(_callback) {
	$.loadingpage.hide();
	$.loadingBar.hide();
	$.activityIndicator.hide();
	_callback && _callback();
};

