// var args = $.args;
var API = require('api');
var loading = Alloy.createController("loading");
var AppVersionControl = require('AppVersionControl');
AppVersionControl.checkAndUpdate();

var menu_button = [
	{classes:['wfill','bold','menu_item'], height: 60, id: "notification", target: "notification/index", onClick: navTo, title: "NOTIFICATION"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "e-guide", onClick: openEguide, url: "api/getGuide", title: "STAFF MANUAL"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "entitlement", target: "claim/index", onClick: navTo, title: "ENTITLEMENT"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "e-claim", target: "claim/submit", onClick: navTo, title: "E-CLAIM"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "e-claim2", target: "claim/submit2", onClick: navTo, title: "E-CLAIM"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "medical_reports", target: "medical_reports/index", onClick: navTo, title: "MEDICAL REPORTS"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "call_hr", target: "hr", onClick: call, title: "CALL HR"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "email_hr", target: "feedback/index", to: "hr", onClick: navTo, title: " EMAIL HR"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "call_valor", target: "valor", onClick: call, title: "CALL FLEXBEN"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "email_valor", target: "feedback/index", to: "support", onClick: navTo, title: "EMAIL FLEXBEN"},


	{classes:['wfill','bold','menu_item'], height: 60, id: "e-card", target: "ecard/index", onClick: navTo, title: "E-CARD"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "panel_clinic", target: "map/index", param:{type: "clinic", page_title: "PANEL CLINIC"},onClick: navTo, title: "PANEL CLINIC"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "panel_hospital",target: "map/index", param:{type: "hospital", page_title: "PANEL HOSPITAL"}, onClick: navTo, title: "PANEL HOSPITAL"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "employee_topup", target: "employee_topup/index", onClick: navTo, title: "EMPLOYEE TOPUP"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "e-referral", target: "referral/index", onClick: navTo, title: "E-REFERRAL"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "request_gl", onClick: GLPopup, title: "REQUEST GL"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "e-gl", target: "egl/index", onClick: navTo, title: "E-GL"},
	{classes:['wfill','bold','menu_item'], height: 60 ,id: "e-news", onClick: openEguide, url: "api/getNews", title: "E-NEWS"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "redemption", target: "redemption/index", onClick: navTo, title: "REDEMPT ION"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "e-points", target: "epoints/index", onClick: navTo, title: "E-POINTS"},
	{classes:['wfill','bold','menu_item'], height: 60, id: "health_profile", target: "health_profile/index", onClick: navTo, title: "HEALTH RECORD"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "e-orders", target: "eorders/index", onClick: navTo, title: "E-ORDER"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "flexi_benefit", target: "webview", onClick: navTo, title: "E-SELECTION", param:{url: "https://www.flexben.my/main/choosePackage?u_id="+Ti.App.Properties.getString("u_id") || "", title: "FLEXI-BENEFIT"}},
    {classes:['wfill','bold','menu_item'], height: 60, id: "ask_medical_team", target: "ask_med/submit", onClick: navTo, title: "ASK MED"},

    //{classes:['wfill','bold','menu_item'], height: 60, id: "ask_medical_team", target: "webview", onClick: navTo, title: "ASK MEDICAL TEAM", param:{url: "https://www.flexben.my/main/askMedicalTeam?is_app=1&u_id="+Ti.App.Properties.getString("u_id") || "", title: "ASK MEDICAL TEAM"}},
    {classes:['wfill','bold','menu_item'], height: 60, id: "covid-19", target: "webview", onClick: navTo, title: "COVID-19", param:{url: "https://www.flexben.my/main/COVID19?is_app=1", title: "COVID-19"}},
    {classes:['wfill','bold','menu_item'], height: 60, id: "pre_employment", target: "pre_employment/index", onClick: navTo, title: "PRE-EMPLOYMENT"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "vitality", target: "vitality/submit", onClick: openVitality, title: "VITALITY"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "e-order2", target: "eorder2/submit", onClick: navTo, title: "E-ORDER"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "e-insurance", target: "einsurance/submit", onClick: navTo, title: "E-INSURANCE"},
    {classes:['wfill','bold','menu_item'], height: 60, id: "e-gl2", target: "egl2/submit", onClick: navTo, title: "E-GL"},
		{classes:['wfill','bold','menu_item'], height: 60, id: "logout", onClick: logout, title: "LOGOUT"},
];

function logout(){
	Ti.App.Properties.removeAllProperties();
	$.win.close();
}

var PUSH = require('enablePush');
PUSH.pushNotification();

function init(){
	$.win.add(loading.getView());
	render_button();
	render_user_info();
	$.version.text = "Ver "+Titanium.App.version;
	//refresh();
}

init();
var health_profile = false;
function render_button(){
	var u_id = Ti.App.Properties.getString("u_id") || "";
	var params = {u_id: u_id};
	$.menu.removeAllChildren();
	API.callByPost({url: "api/checkPermission", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			for (var i=0; i < menu_button.length; i++) {
				for (var j=0; j < result.data.length; j++) {
				    if(result.data[j] == "health_profile"){
				        health_profile = true;
				    }
					if(result.data[j] == menu_button[i].id){
						//var button = $.UI.create("Button", menu_button[i]);
						var view_button = $.UI.create("View", menu_button[i]);
						var left_right_indicator = $.UI.create("View", {classes:['wfill','hsize'], backgroundColor: "#00a6c9"});
						var label_button = $.UI.create("Label", {classes:['wfill','hsize','h5','bold'], color: "#ffffff", left: 10, right:10, textAlign:"center", text: menu_button[i].title, touchEnabled: false});
						left_right_indicator.add(view_button);
						view_button.add(label_button);
						$.menu.add(left_right_indicator);
						view_button.addEventListener("click", menu_button[i].onClick);
					}
				};
				if(typeof (menu_button[i].debug) != "undefined"){
					//var button = $.UI.create("Button", menu_button[i]);
					var view_button = $.UI.create("View", menu_button[i]);
					var left_right_indicator = $.UI.create("View", {classes:['wfill','hsize'], backgroundColor: "#00a6c9"});
					var label_button = $.UI.create("Label", {classes:['wfill','hsize','h5','bold'], color: "#ffffff", left: 10, right:10, textAlign:"center", text: menu_button[i].title, touchEnabled: false});
					left_right_indicator.add(view_button);
					view_button.add(label_button);
					$.menu.add(left_right_indicator);
					view_button.addEventListener("click", menu_button[i].onClick);
				}
			};
			if(result.data2.link){
				navTo({target: "webview", param: {url: result.data2.link, title: "Vitality"}});
			}
		}
	});

}

//init private function start

function render_user_info(){
	if(Ti.App.Properties.getString("name") != ""){
		$.user_info.add($.UI.create("Label", {classes:['wfill','hsize','h5'], textAlign: "left", text: Ti.App.Properties.getString("name")}));
	}
	if(Ti.App.Properties.getString("master") != ""){
		$.user_info.add($.UI.create("Label", {classes:['wfill','hsize','h5'], textAlign: "left", text: "NRIC: "+Ti.App.Properties.getString("master")}));
	}
	if(Ti.App.Properties.getString("empno") != ""){
		$.user_info.add($.UI.create("Label", {classes:['wfill','hsize','h5'], textAlign: "left", text: "Staff No: "+Ti.App.Properties.getString("empno")}));
	}
	if(Ti.App.Properties.getString("location") != null){
		$.user_info.add($.UI.create("Label", {classes:['wfill','hsize','h5'], textAlign: "left", text: "Subsidiary Location: "+Ti.App.Properties.getString("location")}));
	}
	$.company_logo.image =  Ti.App.Properties.getString("company_logo");
	$.company_name.text = Ti.App.Properties.getString("company_name");
}

function GLPopup(){
	var dialog = Ti.UI.createOptionDialog({
		options: ['Inpatient Care','Outpatient Care']
	});
	dialog.show();
	dialog.addEventListener("click", function(ex){
		if(ex.index == 0){
			navTo({target: "egl/inpatient"});
		}else if(ex.index == 1){
			navTo({target: "egl/outpatient"});
		}
	});
}
//refresh private function end

function navTo(e){

	var source = (typeof e.source != "undefined")?e.source:e;
	var param = (typeof source.param != "undefined")?source.param:{};
	if(typeof source.to != "undefined"){
		_.extend(param, {to: source.to});
	}
	var win = Alloy.createController(source.target, param || {}).getView();
	var target = source.target;
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}


function openVitality(e){
	var moment = require('alloy/moment');
	var vitality_daily = Ti.App.Properties.getString("vitality_daily") || false;
	if(vitality_daily && vitality_daily == moment().format("YYYY-MM-DD")){
		popupHealth(e.source);
	}else{
		var container = $.UI.create("View", {classes: ["wfill", 'hsize','vert', 'rounded'], backgroundColor:"#fff", left: 20, right:20});
		var lbl_tnc = $.UI.create("Label", {classes: ["wfill", 'hsize','h4', 'padding'], text: "Reliance at own risk-You undertake the activities at your own risk. The activities available through the Vitality Program (including the information and materials provided through the Vitality Program) may not be suitable for all Bunge Staffs. You must use your best endeavours to participate in the activities safely and avoid harm to yourself. You must seek the advice of your doctor if you are uncertain about the impacts of an activity on your health or are uncertain about any recommendations made as a result of an activity"});
		var btn_agree = $.UI.create("Button", {classes: ["small_button"], height: 40, width:160, bottom:"10", title: "Understand and Agree"});
		container.add(lbl_tnc);
		container.add(btn_agree);
		$.win.add(container);
		btn_agree.addEventListener("click", function(){
			Ti.App.Properties.setString("vitality_daily", moment().format("YYYY-MM-DD"));
			$.win.remove(container);
			popupHealth(e.source);
		});
	}
}

function popupHealth(source){
	var dialog_list = (OS_IOS)?['Apple Health', "Challenges Update", "Cancel"]:["MynutriDiari 2", "Challenges Update", "Cancel"];
	var dialog = Ti.UI.createOptionDialog({
		cancel: 2,
		options: dialog_list
	});
	dialog.show();
	dialog.addEventListener("click", function(ex){
		if(ex.index == 1){
			navTo(source);
		}else if(ex.index == 0){
			Ti.Platform.openURL((OS_IOS)?"x-apple-health://":"https://play.google.com/store/apps/details?id=my.gov.moh.mynutridiariandroid&hl=en");
		}
	});
}

function openEguide(e){
    var source = (typeof e.source != "undefined")?e.source:e;
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	var params = {u_id: u_id};
	API.callByPost({url: source.url, params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(result.status == "success"){
				var guidebooks = _.pluck(result.data, "name");
				guidebooks.push("Cancel");
				var dialog = Ti.UI.createOptionDialog({
					cancel: (guidebooks.length > 0)?guidebooks.length - 1:0,
					options: guidebooks
				});
				dialog.show();
				dialog.addEventListener("click", function(ex){
					if((OS_IOS)?ex.cancel != ex.index:!ex.cancel){
						var param_url = (OS_IOS)?result.data[ex.index].link:"https://docs.google.com/gview?embedded=true&url="+result.data[ex.index].link;
						navTo({target: "webview", param: {url: param_url, title: result.data[ex.index].name}});
					}
				});
			}else{
				alert(result.data);
			}
		},
		onfinish: function(){
			loading.finish();
		}
	});
}

function call(e){
	var number = (e.source.target == "valor")?Ti.App.Properties.getString("flexben_contact"):Ti.App.Properties.getString("company_contact");
	number = (number != null)?number.split("/"):[];
	if(number.length > 1){
	    number.push("Cancel");
		var dialog = Ti.UI.createOptionDialog({
			options: number
		});
		dialog.show();
		dialog.addEventListener("click", function(ex){
		    console.log(number[ex.index]);
		    if(number[ex.index] == "Cancel" || number[ex.index] == "undefined" || typeof number[ex.index] == "undefined"){
		        return;
		    }
			var tel = number[ex.index].replace(/[+]/g, "");
			tel = tel.replace(/-/g, "");
			tel = tel.trim();
			//var tel = "0122268384";
			Ti.Platform.openURL("tel:"+tel);//+tel.replace(/-/g, ""));
		});
	}else if(number.length == 1){
	    var tel = number[0].replace(/[+]/g, "");
        tel = tel.replace(/-/g, "");
        tel = tel.trim();
		Ti.Platform.openURL("tel:"+tel);
	}else{
		alert("No contact found");
	}
}

//	require("step_service");

$.win.addEventListener("postlayout", function(){
	if(OS_ANDROID){
	   /* if(health_profile){
    	       var step = require("step_service");
            	var intent = Titanium.Android.createServiceIntent({
            	  url: 'step_service.js'
            	});
            	if (!Ti.Android.isServiceRunning(intent)) {
            	    Ti.Android.startService(intent);
            	} else {
            	}
        	}*/
	}
});

$.win.addEventListener("close", function(){
	Ti.App.removeEventListener("refresh_home", render_button);
});

Ti.App.addEventListener("refresh_home", render_button);


Ti.App.Properties.removeProperty("cart");
var Cloud = require('ti.cloud');

if(OS_IOS){
    //var step = require("step_service");
    //step.recentSteps();
	// Register for push notification
	Ti.Network.registerForPushNotifications({
	    // Only need to listen to alerts
	    types: [Ti.Network.NOTIFICATION_TYPE_ALERT],
	    success: deviceTokenSuccess,
	    error: deviceTokenError
	});

	function deviceTokenSuccess(e) {

	    // Subscribes the device to the 'silent_push' channel to listen for silent push alerts
	    // The channel name is arbitrary and can be anything you wish
	    Cloud.PushNotifications.subscribeToken({
	        device_token: e.deviceToken,
	        channel: 'silent_push',
	        type: 'ios'
	    }, function (e) {
	        if (e.success) {
	            Ti.API.info('Subscribed');
	        } else {
	            Ti.API.info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
	        }
	    });
	}

	function deviceTokenError(e) {
	    Ti.API.info('Failed to register for push notifications: ' + e.error);
	}

	Ti.App.iOS.addEventListener('silentpush', function(e) {
	    // Initiate a download operation
	    // Put the application back to sleep
	    console.log("silent push");
	    console.log("why");
		step.recentSteps();
		console.log("why");
		Ti.App.iOS.endBackgroundHandler(e.handlerId);
		console.log("why");
	});
}
