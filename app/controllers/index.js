var loading = Alloy.createController("loading");
var API = require('api');
var COMMON = require('common');
function init(){
	var u_id = Ti.App.Properties.getString("u_id") || 0;
	$.win.add(loading.getView());
	$.win.open();
    cheat_apple();
	if(u_id){
		var params = {};
		params["u_id"] = u_id;
		params["app_version"] = Titanium.App.version;
		params["device_os"] = Titanium.Platform.osname+" "+Titanium.Platform.version;
		params["device_model"] = Titanium.Platform.model+" "+Titanium.Platform.ostype;
		params["device_macaddress"] = Titanium.Platform.macaddress;

		API.callByPost({url: "api/validateUser", params: params}, {
			onload: function(responseText){
				var result = JSON.parse(responseText);
				if(result.status == "error"){
					COMMON.createAlert("User Validate Error", result.data[0], loading.finish());
				}else{
					_.each(result.data, function(value, key){
					    Ti.App.Properties.setString(key, value);
					});
					if(OS_ANDROID){
						var win = Alloy.createController("home").getView();
						win.open();
					}else{
						var win = Alloy.createController("drawer").getView();
						win.open();
					}
					
				}

			}, onfinish: function(){
				loading.finish();
			}
		});
	}
}

init();

function cheat_apple(){
    API.callByPost({url: "api/getIsShowRegister"}, {
        onload: function(responseText){
            var result = JSON.parse(responseText);
            if(result.status == "success" && result.data != 1){
                $.register_button.hide();
                $.register_button.width = 0;
            }
        }
    });
}

function do_login(){
	var childs = $.forms.getChildren();
	var params = {};
	for (var i=0; i < childs.length; i++) {
		if(childs[i].mandatory){
			if(childs[i].value == ""){
				alert(childs[i].hintText+" cannot be empty");
				return;
			}else{
				params[childs[i].id] = childs[i].value;
			}
		}else{
			params[childs[i].id] = childs[i].value;
		}
	};
	params["app_version"] = Titanium.App.version;
	params["device_os"] = Titanium.Platform.osname+" "+Titanium.Platform.version;
	params["device_model"] = Titanium.Platform.model+" "+Titanium.Platform.ostype;
	params["device_macaddress"] = Titanium.Platform.macaddress;
	loading.start();
	API.callByPost({url: "api/doLogin", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(result.status == "error"){
				COMMON.createAlert("Login Failed", result.data[0], loading.finish());
			}else{
				_.each(result.data, function(value, key){
				    Ti.App.Properties.setString(key, value);
				});
				if(OS_IOS){
					var win = Alloy.createController("drawer").getView();
					win.open();
				}else{
					var win = Alloy.createController("home").getView();
					win.open();
				}
				
			}

		}, onfinish: function(){
			loading.finish();
		}
	});
}

function navTo(e){

	if(OS_IOS){
		var nav_window = Titanium.UI.iOS.createNavigationWindow();
		//var nav_window = $.UI.create("NavigationWindow", {role: "centerWindow"});
		var win = Alloy.createController(e.source.target, {nw: nav_window}).getView();
		nav_window.window = win;
		nav_window.open();
	}else{
		var win = Alloy.createController(e.source.target).getView();
		win.open();
	}
}
