var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var u_id = Ti.App.Properties.getString("u_id") || "";
var API = require('api');
var moment = require('alloy/moment');

function camera_callback(event){
	console.log((640 / event.media.width)+" "+(640 / event.media.height));
	var new_height = (event.media.height <= event.media.width)?event.media.height*(640 / event.media.width):640;
	var new_width = (event.media.width <= event.media.height)?event.media.width*(640 / event.media.height):640;
	var blob = event.media;

	console.log(" "+event.media.width+" "+event.media.height);
	console.log(new_width+" "+new_height);
	blob = blob.imageAsResized(new_width, new_height);
    var img = Ti.UI.createImageView({image: blob, height:120, width: Ti.UI.SIZE, filedata:blob});
	//var img = $.UI.create("ImageView", {width: 120, classes:['hsize'], image: blob});
	$.image_preview.add(img);
	$.image_preview.parent.attached = 1;
}

function init(){
	$.win.add(loading.getView());
	$.camera.init({callback: camera_callback, source: $.camera});
	refresh({});
}
init();

function refresh(e){
    var source = (typeof e.source != "undefined")?e.source:e;
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}

	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: "api/getProductList", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			$.listing.removeAllChildren();
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function doCall(){
	Titanium.Platform.openURL('tel: 123456789');
}

function doEmail(){
	var emailDialog = Ti.UI.createEmailDialog()
	emailDialog.subject = "E-order request";
	emailDialog.toRecipients = ['aapharmacy@yahoo.com'];
	emailDialog.messageBody = '';
	emailDialog.open();
}

function render_list(arr){
	console.log(arr.length+" arr.length");
	var pw = Ti.Platform.displayCaps.platformWidth;
    var ldf = Ti.Platform.displayCaps.logicalDensityFactor;
	var pwidth = (OS_IOS)?pw:parseInt(pw / (ldf || 1), 10);
	for (var i=0; i < arr.length; i++) {
		if(arr[i].start_date <= moment(new Date()).format("YYYY-MM-DD") && arr[i].publish == 1){
			var view_container = $.UI.create("View", {classes:['hfill','bg_light_blue'], top:10, right:10, bottom:10, width: (pwidth - 30) / 2.5, bottom: 0});
			var image_order = $.UI.create("ImageView", {classes:['wsize'], height: 150, bottom:10, image: arr[i].img_path});
			var view_text = $.UI.create("View", {classes:['wfill','hsize','vert'],backgroundColor: "#8042434b", bottom:0});
			var label_package_name = $.UI.create("Label", {classes:['wfill','hsize','padding'], bottom:0, text: arr[i].name});
			var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
			var text_address = arr[i].add1+" "+arr[i].add2+" "+arr[i].city+" "+arr[i].state;
			var label_retail_price = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], textAlign: "left", top:0, text: "RM "+arr[i].retail_price});
			var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
			var button_map = $.UI.create("Button", {classes:['wfill'], height: 40, left: 10, right:10, bottom:10, url:"eorders/detail",  title: "ORDER",  package_id: arr[i].id});

			view_container.add(image_order);
			//view_container.add(view_hr);
			view_text.add(label_package_name);
			view_text.add(label_retail_price);
			//view_action.add(button_map);
			view_container.add(view_text);
			$.listing.add(view_container);

			//button_map.addEventListener("click", navTo);
		}
	};
}

function loadComboBox(e){
	console.log(u_id+" uid");
	var indicator = $.UI.create("ActivityIndicator", {classes:['wsize','hsize'], style: Ti.UI.ActivityIndicatorStyle.DARK,});
	indicator.show();
	e.source.add(indicator);
	var params = {u_id: u_id};
	API.callByPost({url: e.source.url, params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(result.status == "success"){
				e.source.data = result.data;
			}else{
				e.source.data = [];
			}
		}, onfinish: function(){
			e.source.opacity = 1;
			e.source.touchEnabled = true;
			indicator.hide();
		}
	});
}

function hintTextFocusHack(e){
	console.log(e.source.hintText);
	if(e.source.value == e.source.hintText){
        e.source.value = "";
        e.source.color = "#fff";
    }
}

function hintTextBlurHack(e){
	if(e.source.value==""){
        e.source.value = e.source.hintText;
        e.source.color = "#fff";
    }
}

function datePicker(e){
	var val_date = (typeof e.source.date != "undefined")?e.source.date:new Date();
	var view_container = $.UI.create("View", {classes:['wfill', 'hfill'], zIndex: 50,});
	var mask = $.UI.create("View",{
		classes:['wfill','hfill'],
		backgroundColor: "#80000000"
	});
	var view_box = $.UI.create("View", {classes:['wfill','hsize','vert'], backgroundColor: "#32323e", zIndex: 50});
	var picker = $.UI.create("Picker", {
		classes:['hsize'],
		type:Ti.UI.PICKER_TYPE_DATE_AND_TIME,
		value: val_date,
		color: "#ffffff",
		backgroundColor: "Transparent",
		//dateTimeColor: "#ffffff",
		top: 30,
		bottom: 30,
	});
	var ok_button = $.UI.create("Button", {classes:['wfill'], borderRadius:0, height: 50, title: "SELECT A DATE / TIME"});
	view_box.add(picker);
	view_box.add(ok_button);
	view_container.add(view_box);
	view_container.add(mask);
	$.win.add(view_container);

	mask.addEventListener("click", function(){
		$.win.remove(view_container);
	});

	ok_button.addEventListener("click", function(ex){
		var dd = picker.value.getDate();
		var mm = picker.value.getMonth()+1;
		var yyyy = picker.value.getFullYear();
		e.source.value = yyyy+'-'+mm+'-'+dd;
		e.source.date = picker.value;
		e.source.children[0].text = dd+'/'+mm+'/'+yyyy;
		$.win.remove(view_container);
	});
}

function openTnc(){
	var win = Alloy.createController("webview", {url: "https://flexben.my/main/askMedicalTNC", title: "Terms and Conditions"} || {}).getView();
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

function popout(e){
	var options_arr = _.pluck(e.source.data, e.source.option_name);
	options_arr.push("Cancel");;
	var dialog = Ti.UI.createOptionDialog({
		cancel: (options_arr.length > 0)?options_arr.length - 1:0,
		options: options_arr,
		selectedIndex: e.source.value || 0,
		title: e.source.children[0].text
	});

	dialog.show();
	dialog.addEventListener("click", function(ex){
		console.log(ex.index+" "+ex.cancel);
		if((OS_IOS)?ex.cancel != ex.index:!ex.cancel){
			e.source.children[0].text = options_arr[ex.index];
			e.source.value = e.source.data[ex.index][e.source.option_key];
		}
	});
}

function tncCheck(e){
	e.source.backgroundColor = (e.source.parent.value)?"#ffffff":"#2277ff";
	e.source.parent.value = (e.source.parent.value)?0:1;
	console.log(e.source.parent);
}

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {

		if(typeof forms_arr[i].ignore != "undefined"){

		}else{
			if(forms_arr[i].required && forms_arr[i].value == ""){
				console.log(_.isUndefined(forms_arr[i].value)+" _.isEmpty(forms_arr[i].value)");
				error_message += forms_arr[i].hintText+" cannot be empty\n";
			}

			if(forms_arr[i].format == "photo"){
				var photo_arr = $.image_preview.getChildren();
				var files_arr = [];
				console.log(photo_arr.length+" how many photo");
				for (var j=0; j < photo_arr.length; j++) {
				  //files_arr.push(photo_arr[j].filedata);
				  var key = "Filedata"+((j)?j:"");
				  console.log(key);
				  console.log("_.extend(params, {"+key+": photo_arr[j].filedata})");
				  eval("_.extend(params, {"+key+": photo_arr[j].filedata})");
				  //_.extend(params, {key: photo_arr[j].filedata});
				};

			}else if(forms_arr[i].format == "photo" && !forms_arr[i].attached){
				error_message += "Please upload your receipt\n";
			}else if(forms_arr[i].id == "tnc" && !forms_arr[i].value){
				console.log(forms_arr[i].value);
				error_message += "Please read and agreed the terms and conditions\n";
			}else{
				params[forms_arr[i].id] = forms_arr[i].value;
			}
		}
	};

	if(error_message != ""){
		alert(error_message);
		return;
	}
	params["u_id"] = u_id;
	console.log(params);
	loading.start();
	API.callByPost({url: "api/addAskMed", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);

			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?result.message:result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
					$.win.close();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function closeWindow(){
	$.win.close();
}

var counter = 0;

$.win.addEventListener("postlayout", function(){
	return;
	console.log("two time?");
	var corpcode = Ti.App.Properties.getString('corpcode');
	console.log(corpcode+" corpcode");
	if(corpcode == "BLE" || corpcode == "BLC"){
		if(counter < 1){
			var win = Alloy.createController("webview", {url: "https://www.flexben.my/public/images/BUNGE_CLAIM_NOTICE.png", title: "Claim Checklist"}).getView();
			if(OS_IOS){
				Alloy.Globals.drawer.centerWindow.openWindow(win);
			}else{
				win.open();
			}
			counter ++;
		}
	}
});
