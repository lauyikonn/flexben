var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.title = args.page_title;
	$.win.add(loading.getView());
	refresh({});
}
init();

function refresh(e){
	var url = "api/getPreEmploymentList";
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	console.log(u_id+" uid"+url);
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	API.callByPost({url: url, params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if((typeof result.data != "undefined" && result.data.length > 0)){
				render_employment_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_employment_list(arr){
	$.clinic_list.removeAllChildren();
	console.log(arr);
	console.log(arr.length+" arr.length");
	if(arr.length <= 0){
	    $.clinic_list.add($.UI.create("Label", {classes:['wfill','hsize','h4','padding'], text: "No Result found"}));
	}
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize','padding'], bottom:5, text: arr[i].name});
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
		var id = addRow({title: "ID", value: arr[i].id});
		var final_gl_amount = addRow({title: "FINAL GL AMOUNT", value: arr[i].final_gl_amount});
		var visit_date = addRow({title: "VISIT DATE", value: arr[i].visit_date});
		var sailing_date = addRow({title: "SAILING DATE", value: arr[i].sailing_date});
		var hospital_name = addRow({title: "HOSPITAL", value: arr[i].hospital_name});
		var hospital_procedure = addRow({title: "HOSPITAL PROCEDURE", value: arr[i].hospital_procedure});
		//var button_map = $.UI.create("Button", {width: "40%", left: 10, bottom:10,  title: "GET DIRECTION", text_address:text_address, coord: arr[i].latitude+", "+arr[i].longitude, tel: arr[i].tel});
		var view_detail = $.UI.create("View", {classes:['wfill','hsize','horz']});
		view_container.add(label_name);
		view_container.add(view_hr);
		view_container.add(view_detail);
		view_detail.add(id);
		view_detail.add(final_gl_amount);
		view_detail.add(visit_date);
		view_detail.add(sailing_date);
		view_detail.add(hospital_name);
		view_detail.add(hospital_procedure);
		
		for (var y=0; y < arr[i].attachment.length; y++) {
			var pdf_icon = $.UI.create("ImageView", {width: 30, height: 30, left: 10, bottom:10, image: "images/icons/pdf.png", info: arr[i].attachment[y]});
			view_detail.add(pdf_icon);
			pdf_icon.addEventListener("click", navTo);

		};
		
		$.clinic_list.add(view_container);
		
	};
}

function navTo(e){
	//e.source.info.title
	var win = Alloy.createController("webview", {url: e.source.info.link, title: e.source.info.title}).getView();
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

function addRow(e){
	var text = $.UI.create("Label", {classes:['bold','wfill','hsize','padding'], text: e.title});
	var value = $.UI.create("Label", {classes:['wfill','hsize','padding'], top:0, text: e.value || "-"});
	var view = $.UI.create("View", {classes:['hsize','vert'], width: "49%"});
	view.add(text);
	view.add(value);
	return view;
}

function url_scheme(e){
	Ti.Platform.openURL((OS_IOS)?"Maps://http://maps.google.com/maps?q="+e.source.text_address+"&hl=en&daddr="+e.source.coord+"&spn="+e.source.coord+"&daddr="+e.source.coord:"http://maps.google.com/maps?q="+e.source.text_address+"&hl=en&daddr="+e.source.coord+"&spn="+e.source.coord+"&daddr="+e.source.coord);
}

function tel_call(e){
	var tel = e.source.tel.replace(/[+]/g, "");
	Ti.Platform.openURL('tel:+'+tel);
	Ti.Platform.openURL("tel:"+tel);
}

function closeWindow(e){
	$.win.close();
}

