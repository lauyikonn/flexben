var args = arguments[0] || {};
var loading = Alloy.createController("loading");

var platformHeight = percentHeight("100");
var platformWidth = percentWidth("100");
var store_type = "";

var pin_data = [
	{store_type: "", name: "Show All", pin_number: 0},
	{store_type: 1, name: "Fashion > Clothing", pin_number: 2},
	{store_type: 2, name: "Fashion > Shoes & Bags", pin_number: 2},
	{store_type: 3, name: "Fashion > Accessories", pin_number: 2},
	{store_type: 4, name: "Home & Living > Furniture", pin_number: 12},
	{store_type: 5, name: "Home & Living > Electrical", pin_number: 12},
	{store_type: 6, name: "Home & Living > Decor & Improvement", pin_number: 12},
	{store_type: 7, name: "Kids & Baby", pin_number: 3},
	{store_type: 8, name: "Mobile & Computers", pin_number: 4},
	{store_type: 9, name: "Electronics", pin_number: 5},
	{store_type: 10, name: "Pets", pin_number: 6},
	{store_type: 11, name: "Heakth & Beauty > Salon & Services", pin_number: 7},
	{store_type: 12, name: "Heakth & Beauty > Beauty Supply", pin_number: 7},
	{store_type: 13, name: "Heakth & Beauty > Spa & Message", pin_number: 7},
	{store_type: 14, name: "Automotive > Cars", pin_number: 1},
	{store_type: 15, name: "Automotive > Motorbikes", pin_number: 1},
	{store_type: 16, name: "Restaurant > Chinese", pin_number: 8},
	{store_type: 17, name: "Restaurant > Malay", pin_number: 8},
	{store_type: 18, name: "Restaurant > Indian", pin_number: 8},
	{store_type: 19, name: "Restaurant > Japanese", pin_number: 8},
	{store_type: 20, name: "Restaurant > Korean", pin_number: 8},
	{store_type: 21, name: "Restaurant > Thai", pin_number: 8},
	{store_type: 22, name: "Restaurant > International / Western", pin_number: 8},
	{store_type: 23, name: "Restaurant > Cafe", pin_number: 8},
	{store_type: 24, name: "Restaurant > Bar & Bistro", pin_number: 8},
	{store_type: 25, name: "Leisure > Sports", pin_number: 9},
	{store_type: 26, name: "Leisure > Outdoor Parks", pin_number: 9},
	{store_type: 27, name: "Entertainment", pin_number: 10},
	{store_type: 28, name: "Travel & Hotel", pin_number: 11}
];
console.log(platformHeight - 130);
//$.mapview.height = platformHeight - 130;

function distance(lat1, lon1, lat2, lon2) {
  var p = 0.017453292519943295;    // Math.PI / 180
  var c = Math.cos;
  var a = 0.5 - c((lat2 - lat1) * p)/2 + 
          c(lat1 * p) * c(lat2 * p) * 
          (1 - c((lon2 - lon1) * p))/2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

var saveCurLoc = function(e) {
	console.log("saveCurLoc");
    if (e.error) {
        alert('Location service is disabled. ');
        //COMMON.closeWindow($.location);
    } else {
    	//console.log(e);
    	showCurLoc = true;
    	Ti.App.Properties.setString('latitude', e.coords.latitude);
    	Ti.App.Properties.setString('longitude', e.coords.longitude);
    	$.mapview.region =  {latitude: e.coords.latitude, longitude:e.coords.longitude, zoom: 12, latitudeDelta: 0.01, longitudeDelta: 0.01};
    	//centerMap();
       //console.log(Ti.App.Properties.getString('latitude') + "=="+ Ti.App.Properties.getString('longitude'));
    }
    Ti.Geolocation.removeEventListener('location',saveCurLoc);
}; 

if (Ti.Geolocation.locationServicesEnabled) {
    Ti.Geolocation.accuracy = Ti.Geolocation.ACCURACY_HIGH;
    Ti.Geolocation.addEventListener('location', saveCurLoc);
} 

function setFilter(e){
	$.filter_icon.width = 20;
	$.filter_icon.left = 10;
	$.filter_icon.image = "/images/icons/pin_"+e.source.record.pin_number+".png";
	$.filter_text.text = e.source.record.name;
	openCategory();
	store_type = e.source.record.store_type;
	throttle_centerMap();
}

var skip = 0;
var compare_lat = 0, compare_long = 0;
var last_zoom_distance = 0;
var annotations = [];

var throttle_centerMap = _.throttle(centerMap, 2000);
function centerMap(){
	if(skip <= 0){
		skip++;
		return;
	}
	$.detail.hide();
	var u_id = Ti.App.Properties.getString('u_id') || "";
	var token = Ti.App.Properties.getString('token') || "";
	var bounds = getMapBounds($.mapview.region);
	var zoom_distance = distance(bounds.northWest.lat, bounds.northWest.lng, bounds.southEast.lat, bounds.southEast.lng);
	console.log(last_zoom_distance+" "+zoom_distance);
	var compare_zoom_distance = (last_zoom_distance.toFixed(1) !=  zoom_distance.toFixed(1))?true:false;
	last_zoom_distance = zoom_distance;
	var dist = distance($.mapview.region.latitude, $.mapview.region.longitude, compare_lat, compare_long);
	if(compare_zoom_distance > 0.1){
		console.log(dist+" zoom dist");
		annotations = [];
	}
	console.log(dist+" dist to refresh");
	if(dist > 0.3){
		API.callByPost({url: "vendor/distance", params: {nw_latitude: bounds.northWest.lat, nw_longitude: bounds.northWest.lng, se_latitude: bounds.southEast.lat, se_longitude: bounds.southEast.lng, u_id:u_id, token:token, store_type:store_type}}, {onload: function(responseText){
			if(compare_zoom_distance > 0.1){
				$.mapview.removeAllAnnotations();
			}
		
			console.log(responseText);
			var result = JSON.parse(responseText);
			var data = result.data;
			for (var i=0; i < data.length; i++) {
				var found = _.where(annotations, {id: data[i].id});
				console.log(typeof found+" typeof found");
				console.log(found.length);
				if(found.length <= 0){
					var pin = {id: data[i].id, latitude: data[i].latitude, longitude: data[i].longitude, icon: "images/icons/pin_"+data[i].store_type+".png", title: data[i].name, subtitle: data[i].address, img_path: data[i].img_path, rating: data[i].rating, voucher: data[i].voucher};
					annotations.push(pin);
					render_annotation(pin);
				}
			};
			
		}});
		compare_lat = $.mapview.region.latitude;
		compare_long = $.mapview.region.longitude;
	}
}

function getMapBounds(region) {
    var b = {};
    b.northWest = {}; b.northEast = {};
    b.southWest = {}; b.southEast = {};

    b.northWest.lat = parseFloat(region.latitude) + 
        parseFloat(region.latitudeDelta) / 2.0;
    b.northWest.lng = parseFloat(region.longitude) - 
        parseFloat(region.longitudeDelta) / 2.0;

    b.southWest.lat = parseFloat(region.latitude) - 
        parseFloat(region.latitudeDelta) / 2.0;
    b.southWest.lng = parseFloat(region.longitude) - 
        parseFloat(region.longitudeDelta) / 2.0;

    b.northEast.lat = parseFloat(region.latitude) + 
        parseFloat(region.latitudeDelta) / 2.0;
    b.northEast.lng = parseFloat(region.longitude) + 
        parseFloat(region.longitudeDelta) / 2.0;

    b.southEast.lat = parseFloat(region.latitude) - 
        parseFloat(region.latitudeDelta) / 2.0;
    b.southEast.lng = parseFloat(region.longitude) + 
        parseFloat(region.longitudeDelta) / 2.0;

    return b;
}
var voucher = false;
var marker = false;
function pinClicked(e){
	console.log(e.annotation.latitude+" "+e.annotation.longitude);
	marker = e.annotation;
	$.vendor_img_path.image = e.annotation.img_path;
	$.vendor_name.text = e.annotation.title;
	$.vendor_address.text = e.annotation.subtitle;
	$.rating.removeAllChildren();
	if(e.annotation.voucher === false || e.annotation.voucher.has_grab){
		$.coupon_box.hide();
	}else{
		$.coupon_box.show();
	}
	voucher = e.annotation.voucher;
	console.log("voucher");
	console.log(voucher);
	for(var i=0;i<e.annotation.rating;i++){
		$.rating.add($.UI.create("Label",{
			classes : ["hsize","wsize","h5","fa-star"],
			color: "#ffd863",
			right: 1,
		}));
	}
	setTimeout(function(){
		$.detail.show();
	},1000);
}

function getDirection(){
	
	try {
   		var waze_url = 'waze://?ll='+marker.latitude+','+marker.longitude+'&navigate=yes';
   		if (Ti.Android){
		   	var intent = Ti.Android.createIntent({
				action: Ti.Android.ACTION_VIEW,
				data: waze_url
			});
			Ti.Android.currentActivity.startActivity(intent); 
		}else{
			Ti.Platform.openURL(waze_url);
			console.log(waze_url+" here");
		}
	} catch (ex) { 
		console.log(ex);
		if(OS_IOS){
			Titanium.Platform.openURL('Maps://http://maps.google.com/maps?ie=UTF8&t=h&z=16&daddr='+marker.latitude+','+marker.longitude);
		}else{
			var intent = Ti.Android.createIntent({
				action: Ti.Android.ACTION_VIEW,
				data: 'geo:'+marker.latitudee+','+marker.longitudee+"?q="+marker.title+" (" + marker.address+")"
			});
			Ti.Android.currentActivity.startActivity(intent);
		}
	}
}

function grabCoupon(){
	var u_id = Ti.App.Properties.getString('u_id') || "";
if(u_id == ""){
	var popup_view;
	var login = Alloy.createController("auth/login", {
			parent: $.win, 
			fb: args.fb,
			successCallback: function(){
			console.log("successCallback");
			$.win.remove(popup_view);
			voucherPopup();
		}});
	var contentView = login.getView("theLogin");
	var popup = Alloy.createWidget("popup", {
		title: "Login", 
		submitButtonImage: false,
		content: contentView,
		closeable: false,
		closeView: function(view){
			$.win.remove(view);
		}
	});
	popup_view = popup.getView();
	$.win.add(popup_view);
}else{
	voucherPopup();
}
	
}

function voucherPopup(){
	console.log("click here");
	console.log(voucher);
	var u_id = Ti.App.Properties.getString('u_id') || "";
	var token = Ti.App.Properties.getString('token') || "";
	var view_content = $.UI.create("View", {classes: ['wfill','hsize','vert'],});
	if(typeof voucher.image!="undefined"){
		view_content.add({view: $.UI.create("ImageView", {classes: ['wfill','hsize'], image: voucher.image})});
	}
	if(typeof voucher.description!="undefined"){
		view_content.add($.UI.create("Label", {classes:['wfill','hsize','h5'], text: voucher.description}));
	}
	
	var popup = Alloy.createWidget("popup", {
		title: voucher.name, 
		submitButtonImage: "/images/icons/grab_coupon.png",
		content: view_content,
		closeView: function(view){
			$.win.remove(view);
		},
		successCallback: function(){
			
			console.log("successCallback");
			API.callByPost({url: "favorite/grab_coupon", params: {u_id: u_id, id:voucher.id, token:token}}, {onload: function(responseText){
				console.log(responseText);
			}});
		}
	});
	$.win.add(popup.getView());
}

function render_annotation(annotation){
	var ann = Alloy.Globals.Map.createAnnotation({
		latitude:annotation.latitude,
	    longitude:annotation.longitude,
	    image: annotation.icon,
	    title: annotation.title,
	    subtitle: annotation.subtitle,
	    img_path: annotation.img_path,
	    rating: annotation.rating,
	    voucher: annotation.voucher,
	});
	$.mapview.addAnnotation(ann);
}

function refresh(latest){
	
}


$.win.addEventListener("close", function(){
	$.destroy();
});

$.mapview.addEventListener("regionchanged", throttle_centerMap);
$.mapview.addEventListener("click", pinClicked);

function init(){
	//$.detail.hide();
	$.win.add(loading.getView());
	//$.view_category.width = platformWidth;
	//$.view_category.left = -platformWidth;
	//loadPinCategory();
}

init();

function loadPinCategory(){
	loading.start();
	console.log(pin_data.length);
	var arr_filter = [];
	for (var i=0; i < pin_data.length; i++) {
		var tvr = $.UI.create("TableViewRow", {classes:['wfill','hsize'], record: pin_data[i]});
		var row = $.UI.create("View", {classes:['wsize','hsize'], left: 0, touchEnabled: false});
		var img_pin = $.UI.create("ImageView", {width: 30, height: 30, left:10, image:"/images/icons/pin_"+pin_data[i].pin_number+".png", touchEnabled: false});
		var lab_category_name = $.UI.create("Label", {classes:['wsize','hsize'], left: 50, text: pin_data[i].name, touchEnabled: false});
		row.add(img_pin);
		row.add(lab_category_name);
		tvr.add(row);
		arr_filter.push(tvr);
	};
	$.filter_list.setData(arr_filter);
	loading.finish();
}

var show_category = false;
var duration = 200;
function openCategory(){
	console.log('openCategory');
	if (show_category){
		moveTo = -platformWidth;
		show_category=false;
	}else{
		moveTo="0";
		show_category=true;
	}

	$.view_category.animate({
		left:moveTo,
		duration: duration
	});
}