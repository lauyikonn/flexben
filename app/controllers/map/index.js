var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.title = args.page_title;
	$.win.add(loading.getView());
	refresh({});
}
init();

function refresh(e){
	var url = (args.type == "hospital")?"api/getHospitalLocator":"api/getClinicLocator";
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	console.log(u_id+" uid"+url);
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: url, params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_clinic_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_clinic_list(arr){
	$.clinic_list.removeAllChildren();
	console.log(arr.length+" arr.length");
	if(arr.length <= 0){
	    $.clinic_list.add($.UI.create("Label", {classes:['wfill','hsize','h4','padding'], text: "No Result found"}));
	}
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		var label_clinicName = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: arr[i].clinicName});
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
		var text_address = arr[i].add1+" "+arr[i].add2+" "+arr[i].city+" "+arr[i].state;
		var label_add1 = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: text_address});
		var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
		var button_map = $.UI.create("Button", {width: "40%", left: 10, bottom:10,  title: "GET DIRECTION", text_address:text_address, coord: arr[i].latitude+", "+arr[i].longitude, tel: arr[i].tel});
		
		view_container.add(label_clinicName);
		view_container.add(view_hr);
		view_container.add(label_add1);
		view_action.add(button_map);
		view_container.add(view_action);
		
		if(arr[i].tel != ""){
			var button_tel = $.UI.create("Button", {width: "30%", left: 10, bottom:10, title: "TELEPHONE", tel: arr[i].tel});
			view_action.add(button_tel);
			button_tel.addEventListener("click", tel_call);
		}
		
		$.clinic_list.add(view_container);
		button_map.addEventListener("click", url_scheme);
		
		
	};
}

function url_scheme(e){
	Ti.Platform.openURL((OS_IOS)?"Maps://http://maps.google.com/maps?q="+e.source.text_address+"&hl=en&daddr="+e.source.coord+"&spn="+e.source.coord+"&daddr="+e.source.coord:"http://maps.google.com/maps?q="+e.source.text_address+"&hl=en&daddr="+e.source.coord+"&spn="+e.source.coord+"&daddr="+e.source.coord);
}

function tel_call(e){
	var tel = e.source.tel.replace(/[+]/g, "");
	Ti.Platform.openURL('tel:+'+tel);
	Ti.Platform.openURL("tel:"+tel);
}

function closeWindow(e){
	$.win.close();
}

