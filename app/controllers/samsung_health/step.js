// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
const Activity = require("android.app.Activity");
const HealthDataStore = require("com.samsung.android.sdk.healthdata.HealthDataStore");
const StepCountReader = require("StepCountReader");
const activity = new Activity(Ti.Android.currentActivity);
const mConnectionListener = new HealthDataStore.ConnectionListener(function(){
	
});
var mStepCountObserver;
const mStore = new HealthDataStore(activity, mConnectionListener);
mStore.connectService();
mReporter = new StepCountReader(mStore, mStepCountObserver);

mStepCountObserver = mReporter.StepCountObserver(function(){
	
});
var mCurrentStartTime = mReporter.TODAY_START_UTC_TIME;
console.log(mCurrentStartTime);
//mCurrentStartTime -= StepCountReader.ONE_DAY;
mReporter.requestDailyStepCount(mCurrentStartTime);

/*
var HealthConnectionErrorResult = require("com.samsung.android.sdk.healthdata.HealthConnectionErrorResult");
var StepCount = require("com.samsung.android.sdk.healthdata.HealthConstants.StepCount");
var HealthDataService = require("com.samsung.android.sdk.healthdata.HealthDataService");

var HealthPermissionManager = require("com.samsung.android.sdk.healthdata.HealthPermissionManager");
var PermissionKey = require("com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionKey");
var PermissionResult = require("com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionResult");
var PermissionType = require("com.samsung.android.sdk.healthdata.HealthPermissionManager.PermissionType");
var HealthResultHolder = require("com.samsung.android.sdk.healthdata.HealthResultHolder");

mStore.connectService();
mReporter = new StepCountReader(mStore, mStepCountObserver);

function mStepCountObserver(){
	
}


public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Get the start time of today in local
        mCurrentStartTime = StepCountReader.TODAY_START_UTC_TIME;
        mDayTv.setText(getFormattedTime());

        // Create a HealthDataStore instance and set its listener
        mStore = new HealthDataStore(this, mConnectionListener);

        // Request the connection to the health data store
        mStore.connectService();
        mReporter = new StepCountReader(mStore, mStepCountObserver);

        mBinningListAdapter = new BinningListAdapter();
        mBinningListView.setAdapter(mBinningListAdapter);
    }
*/

function closeWindow(){
	$.window.close();
}
