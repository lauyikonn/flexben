var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var u_id = Ti.App.Properties.getString("u_id") || "";
var API = require('api');
console.log(args);
function init(){
	var title = (args.to == "hr")?"HR":"FLEXBEN";
	$.win.title = "Send "+title+" Email";
	$.win.add(loading.getView());
	$.name.value = Ti.App.Properties.getString("name") || "";
	$.email.value = Ti.App.Properties.getString("email") || "";
	$.mobile.value = Ti.App.Properties.getString("mobile") || "";
	//refresh();
}
init();

function hintTextFocusHack(e){
	console.log(e.source.hintText);
	if(e.source.value == e.source.hintText){
        e.source.value = "";
        e.source.color = "#ffffff";
    }
}

function hintTextBlurHack(e){
	if(e.source.value==""){
        e.source.value = e.source.hintText;
        e.source.color = "#ffffff";
    }
}

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {to: args.to};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {
		console.log(forms_arr[i].id+" "+forms_arr[i].value);
		if(forms_arr[i].required && forms_arr[i].value == ""){
			error_message += forms_arr[i].id+" cannot be empty\n";
		}
		params[forms_arr[i].id] = forms_arr[i].value;
	};
	if(error_message != ""){
		alert(error_message);
		return;
	}
	params["u_id"] = u_id;
	console.log(params);
	loading.start();
	API.callByPost({url: "api/addFeedback", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?"Your feedback has been successfully submitted":result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
					$.comment.value = "";
					$.comment.blur();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function closeWindow(){
	$.win.close();
}
