var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var moment = require('alloy/moment');
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh({url: "api/getProductList"});
	refresh_cart();
}

init();

function refresh_cart(){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	$.cart_number.text = cart.length;
}

function refresh(e){
    var source = (typeof e.source != "undefined")?e.source:e;
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}

	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: source.url, params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			$.listing1.removeAllChildren();
			$.listing2.removeAllChildren();
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_list(arr){
	console.log(arr.length+" arr.length");
	var pw = Ti.Platform.displayCaps.platformWidth;
    var ldf = Ti.Platform.displayCaps.logicalDensityFactor;
	var pwidth = (OS_IOS)?pw:parseInt(pw / (ldf || 1), 10);
	for (var i=0; i < arr.length; i++) {
		if(arr[i].start_date <= moment(new Date()).format("YYYY-MM-DD") && arr[i].publish == 1){
			var view_container = $.UI.create("View", {classes:['hsize','vert', 'bg_light_blue'], top:10, right:10, bottom:10, width: (pwidth - 30) / 2, bottom: 0});
			var image_order = $.UI.create("ImageView", {classes:['wfill','hsize'], bottom:10, image: arr[i].img_path});
			var label_package_name = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: arr[i].name});
			var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
			//var text_address = arr[i].add1+" "+arr[i].add2+" "+arr[i].city+" "+arr[i].state;
			var label_retail_price = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], textAlign: "left", top:0, text: "RM "+arr[i].retail_price});
			var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
			var button_map = $.UI.create("Button", {classes:['wfill'], height: 40, left: 10, right:10, bottom:10, url:"eorders/detail",  title: "ORDER",  package_id: arr[i].id});

			view_container.add(image_order);
			view_container.add(view_hr);
			view_container.add(label_package_name);
			view_container.add(label_retail_price);
			view_action.add(button_map);
			view_container.add(view_action);
			if(i%2){
				$.listing1.add(view_container);
			}else{
				$.listing2.add(view_container);
			}

			button_map.addEventListener("click", navTo);
		}
	};
}

function doOrder(e){
    loading.start();
    var u_id = Ti.App.Properties.getString("u_id") || "";
    var source = (typeof e.source != "undefined")?e.source:e;
    var params = {u_id: u_id, item_id: source.package_id, quantity: 1};
    API.callByPost({url: "api/doRedeem", params: params}, {
        onload: function(responseText){
            var result = JSON.parse(responseText);
            alert(result.data);
        }, onfinish: function(){
            loading.finish();
        }
    });
}

function navToCart(){
	var win = Alloy.createController("eorders/cart", {}).getView();
	console.log(win);
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

function navTo(e){
	var source = (typeof e.source != "undefined")?e.source:e;
	var win = Alloy.createController(source.url, {id: e.source.package_id}).getView();
	console.log(win);
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

Ti.App.addEventListener("refresh_cart", refresh_cart);

function closeWindow(e){
	Ti.App.removeEventListener("refresh_cart", refresh_cart);
	$.win.close();
}
