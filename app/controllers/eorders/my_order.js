var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var moment = require('alloy/moment');
var status_messge = ["","Waiting for payment", "Waiting to verify", "Waiting for delivery", "Order rejected", "Completed"];
var API = require('api');
function init(){
	//$.win.add(loading.getView());
	refresh();
}
init();

function refresh(e){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	API.callByPost({url: "api/getOrderList", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			if((typeof result.data != "undefined")){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}


function render_list(arr){
	console.log(arr.length+" arr.length");
	console.log(arr);
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {id: arr[i].id, classes:['wfill','hsize','vert'], bottom: 10, backgroundColor: "#fff"});
		var view_product_info = $.UI.create("View", {touchEnabled: false, classes:['wfill', 'hsize']});
		var image_order = $.UI.create("ImageView", {touchEnabled: false, classes:['box','hsize'], height:50, width: 50, left:10, top:10, bottom:10, borderColor:"#995a00", image: arr[i].product[0].img_path});
		var view_product_detail = $.UI.create("View", {touchEnabled: false, classes:['wfill', 'hsize', 'vert'], left: 70});
		var label_name = $.UI.create("Label", {touchEnabled: false, classes:['wfill','hsize','h5'], textAlign:"right", right:10, maxLines: 1, text: arr[i].product[0].item_name});
		var label_quantity = $.UI.create("Label", {touchEnabled: false, classes:['wfill','hsize','h6'], color: "#a1a1a1", textAlign:"right", right:10, maxLines: 1, text: "x"+arr[i].product[0].quantity});
		var label_price = $.UI.create("Label", {touchEnabled: false, classes:['wfill','hsize','h5'], textAlign:"right", right:10, maxLines: 1, text: "RM "+arr[i].product[0].unit_price});
		var view_hr1 = $.UI.create("View", {touchEnabled: false, classes:['hr'], backgroundColor: "#C4DEEC", left: 5, right: 5});
		var view_subinfo = $.UI.create("View", {touchEnabled: false, classes:['wfill','hsize'], top: 5, bottom: 5});
		console.log(parseInt(arr[i].total_product) - 1);
		var label_total_product = $.UI.create("Label", {touchEnabled: false, classes:['hsize','h6'], width: "60%", color: "#a1a1a1", textAlign:"center", left:0, maxLines: 1, text: (arr[i].total_product > 1)?"View "+(parseInt(arr[i].total_product)-1)+" more items":"1 item"});
		var view_hr2 = $.UI.create("View", {touchEnabled: false, height: 30, width: 1, left: "60%", backgroundColor: "#C4DEEC"});
		var label_total = $.UI.create("Label", {touchEnabled: false, classes:['hsize','h6','bold'], width: "40%", color: "#fe2c00", textAlign:"center", right:0, maxLines: 1, text: "Total: RM "+arr[i].total_price});
		var view_hr3 = $.UI.create("View", {touchEnabled: false, classes:['hr'], backgroundColor: "#C4DEEC", left: 5, right: 5});
		var label_status = $.UI.create("Label", {touchEnabled: false, classes:['wfill', 'hsize','h6','padding'],  textAlign:"left", maxLines: 1, text: status_messge[arr[i].status]});
		
		view_container.add(view_product_info);
		view_container.add(view_hr1);
		view_container.add(view_subinfo);
		view_container.add(view_hr3);
		view_container.add(label_status);
		
		view_product_info.add(image_order);
		view_product_info.add(view_product_detail);
		
		view_product_detail.add(label_name);
		view_product_detail.add(label_quantity);
		view_product_detail.add(label_price);
		
		view_subinfo.add(label_total_product);
		view_subinfo.add(view_hr2);
		view_subinfo.add(label_total);
		
		view_container.addEventListener("click", navTo);
		
		$.listing.add(view_container);
	};
}

function navTo(e){
	var win = Alloy.createController("eorders/order_detail", {id: e.source.id}).getView();
	console.log(win);
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();	
	}
}

Ti.App.addEventListener("closeWindow", closeWindow);

function closeWindow(e){
	Ti.App.removeEventListener("closeWindow", closeWindow);
	$.win.close();
}

