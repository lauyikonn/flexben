var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var moment = require('alloy/moment');
var product_detail;
var API = require('api');
function init(){
	refresh({url: "api/getProductDetails"});
	refresh_cart();
}
init();

function refresh_cart(){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	$.cart_number.text = cart.length;
}

function addToCart(){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	product_detail = _.extend(product_detail, {quantity: 1});
	var found = false;
	for(var i=0,j=cart.length; i<j; i++){
		console.log(cart[i]);
		if(cart[i].id == args.id){
			cart[i].quantity++;
			found = true;
		}
	};
	if(!found){
		cart.push(product_detail);
	}

	$.cart_number.text = cart.length;
	console.log(JSON.stringify(cart));
	Ti.App.Properties.setString("cart", JSON.stringify(cart));
	Ti.App.fireEvent("refresh_cart");
}

function navToCart(){
	var win = Alloy.createController("eorders/cart", {}).getView();
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

function buyNow(){
	console.log("buyNow");
	addToCart();
	navToCart();
}

function refresh(e){
    var source = (typeof e.source != "undefined")?e.source:e;
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}

	var params = {u_id: u_id, id: args.id};
	API.callByPost({url: source.url, params: params}, {
		onload: function(responseText){
			console.log("here? not possible ba");
			var result = JSON.parse(responseText);
			if((typeof result.data != "undefined")){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_list(arr){
	product_detail = arr;
	$.img_path.image = arr.img_path;
	$.name.text = arr.name;
	$.retail_price.text = "RM "+arr.retail_price;
	$.description.text = arr.description;
	return;
	for (var i=0; i < arr.length; i++) {
		if(arr[i].start_date <= moment(new Date()).format("YYYY-MM-DD") && arr[i].publish == 1){
			var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
			var image_order = $.UI.create("ImageView", {classes:['wfill','hsize'], bottom:10, image: arr[i].img_path});
			var label_package_name = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: arr[i].name});
			var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
			//var text_address = arr[i].add1+" "+arr[i].add2+" "+arr[i].city+" "+arr[i].state;
			var label_retail_price = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], textAlign: "left", top:0, text: "RM "+arr[i].retail_price});
			var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
			var button_map = $.UI.create("Button", {classes:['wfill'], left: 10, right:10, bottom:10,  title: "ORDER",  package_id: arr[i].id});

			view_container.add(image_order);
			view_container.add(view_hr);
			view_container.add(label_package_name);
			view_container.add(label_retail_price);
			view_action.add(button_map);
			view_container.add(view_action);

			$.listing.add(view_container);

			button_map.addEventListener("click", navTo);
		}
	};
}

function doOrder(e){
    loading.start();
    var u_id = Ti.App.Properties.getString("u_id") || "";
    var source = (typeof e.source != "undefined")?e.source:e;
    var params = {u_id: u_id, item_id: source.package_id, quantity: 1};
    API.callByPost({url: "api/doRedeem", params: params}, {
        onload: function(responseText){
            var result = JSON.parse(responseText);
            alert(result.data);
        }, onfinish: function(){
            loading.finish();
        }
    });
}
function navTo(e){
	var win = Alloy.createController("eorders/detail", {id: e.source.package_id}).getView();
	if(OS_IOS){
		drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

Ti.App.addEventListener("refresh_cart", refresh_cart);

function closeWindow(e){
	Ti.App.removeEventListener("refresh_cart", refresh_cart);
	$.win.close();
}
