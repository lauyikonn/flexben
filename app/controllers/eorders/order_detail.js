var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var moment = require('alloy/moment');
var u_id = Ti.App.Properties.getString("u_id") || "";
var state_arr = [{state_name: "Kuala Lumpur"}, {state_name: "Johor"}, {state_name: "Kedah"}, {state_name: "Kelantan"}, {state_name: "Malacca"}, {state_name: "Negeri Sembilan"}, {state_name: "Pahang"}, {state_name: "Perak"}, {state_name: "Perlis"}, {state_name: "Penang"}, {state_name: "Sabah"}, {state_name: "Sarawak"}, {state_name: "Selangor"}, {state_name: "Terengganu"}];
var API = require('api');
function init(){
	//$.win.add(loading.getView());
	$.camera.init({callback: camera_callback, source: $.camera});
	refresh();
}
init();

function preset_field(e){
	$.full_name.value = e.full_name || "";
	$.address1.value = e.address || "";
	$.address2.value = e.address2 || "";
	$.city.value = e.city || "";
	$.mobile.value = e.mobile || "";
	$.state.value = e.state || "";
	$.state.children[0].text = e.state || "State";
	$.zip.value = e.zip || "";
	$.subtotal.text = "Total: RM "+e.total_price || "Total: RM 0";
	$.image_preview.image = e.receipt_url || "";
	$.courier_name.text = e.courier_name || "-";
	$.tracking_code.text = e.tracking_code || "-";
}

function refresh(e){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(args.id == ""){
		return;
	}

	var params = {id: args.id};
	API.callByPost({url: "api/getOrderDetails", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);

			if((typeof result.data != "undefined")){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function calculate_subtotal(){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	var total = 0;
	for (var i=0; i < cart.length; i++) {
		total += parseFloat(cart[i].retail_price) * parseFloat(cart[i].quantity);
	}
	$.subtotal.text = "Subtotal: RM "+total;
}

function render_list(obj){

	var cart = obj.product;
	preset_field(obj);
	for (var i=0; i < cart.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','horz'], backgroundColor: "#fff", top:10});
		var image_order = $.UI.create("ImageView", {classes:['padding','hsize'], width: 50, height: 50, right:0, left:0, image: cart[i].img_path});
		var view_info = $.UI.create("View", {classes:['wfill','hsize','padding','vert']});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize'], maxLines: 1, text: cart[i].item_name});
		var label_retail_price = $.UI.create("Label", {classes:['wfill','hsize'], maxLines: 1, text: "RM "+cart[i].total_price});
		var label_retail_quantity = $.UI.create("Label", {classes:['wfill','hsize'], maxLines: 1, text: "x"+cart[i].quantity});

		view_container.add(image_order);
		view_container.add(view_info);
		view_info.add(label_name);
		view_info.add(label_retail_price);
		view_info.add(label_retail_quantity);
		$.listing.add(view_container);
	};
}

function camera_callback(event){

    var new_height = (event.media.height <= event.media.width)?event.media.height*(640 / event.media.width):640;
    var new_width = (event.media.width <= event.media.height)?event.media.width*(640 / event.media.height):640;
    var blob = event.media;

    blob = blob.imageAsResized(new_width, new_height);
    $.image_preview.image = blob;
    $.image_preview.parent.filedata = blob;
    $.image_preview.parent.attached = 1;
}

function loadComboBoxOffline(e){
	var indicator = $.UI.create("ActivityIndicator", {classes:['wsize','hsize'], style: Ti.UI.ActivityIndicatorStyle.DARK,});
	//indicator.show();
	//e.source.add(indicator);
	e.source.data = state_arr;
	e.source.opacity = 1;
	e.source.touchEnabled = true;
	//indicator.hide();
}

function popout(e){
	var options_arr = _.pluck(e.source.data, e.source.option_name);
	options_arr.push("Cancel");
	var dialog = Ti.UI.createOptionDialog({
		cancel: options_arr.length - 1,
		options: options_arr,
		//selectedIndex: e.source.value || 0,
		title: e.source.children[0].text
	});

	dialog.show();
	dialog.addEventListener("click", function(ex){

		if(ex.cancel != ex.index){
			e.source.children[0].text = options_arr[ex.index];
			e.source.value = e.source.data[ex.index][e.source.option_key];
			e.source.children[0].color = "#ffffff";
		}
	});
}

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {id: args.id};
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	var total = 0;
	var error_message = "";
	for (var i=0; i < forms_arr.length; i++) {

		if(!forms_arr[i].ignore){

			Ti.App.Properties.setString(forms_arr[i].id, forms_arr[i].value);
			if(forms_arr[i].required && forms_arr[i].value == ""){

				error_message += forms_arr[i].hintText+" cannot be empty\n";
			}
			if(forms_arr[i].format == "photo" && forms_arr[i].attached){
				_.extend(params, {Filedata: forms_arr[i].filedata});
			}else{
				params[forms_arr[i].id] = forms_arr[i].value;
			}
		}
	};
	if(error_message != ""){
		alert(error_message);
		return;
	}
	params["u_id"] = u_id;

	loading.start();
	API.callByPost({url: "api/submitOrder", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);


			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?result.data:result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
		    			Ti.App.Properties.removeProperty("cart");
		    			Ti.App.fireEvent("refresh_cart");
					$.win.close();
					Ti.App.fireEvent("closeWindow");
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function closeWindow(e){
	$.win.close();
}
