var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var moment = require('alloy/moment');

function init(){
	//$.win.add(loading.getView());
	refresh();
}
init();

function refresh(e){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	render_list(cart);
	calculate_subtotal();
}

function calculate_subtotal(){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	var total = 0;
	for (var i=0; i < cart.length; i++) {
		total += parseFloat(cart[i].retail_price) * parseFloat(cart[i].quantity);
	}
	$.subtotal.text = "Subtotal: RM "+total;
}

function render_list(arr){
	console.log("render_list");
	console.log(arr);
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize'], backgroundColor: "#3dabc2", bottom:10});
		var image_order = $.UI.create("ImageView", {classes:['padding'], height: 50, width: 50, right:0, left:0, image: arr[i].img_path});
		var image_delete = $.UI.create("ImageView", {index: i, operator: "delete", width: 20, height: 20, right:10, top: 10, image: "/images/icons/delete_icon.jpg"});
		var view_info = $.UI.create("View", {classes:['wfill','hsize','padding','vert'], left: 70});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize'], maxLines: 1, text: arr[i].name});
		var label_retail_price = $.UI.create("Label", {classes:['wfill','hsize'], maxLines: 1, text: "RM "+arr[i].retail_price});
		var view_quantity = $.UI.create("View", {classes:['wsize', 'hsize', 'horz'], left: 0, backgroundColor: "#ccc"});
		var view_minus = $.UI.create("View", {index: i, operator: "-", width: 20, height: 20,  top: 1, left:1, right:1, bottom:1, backgroundColor: "#3dabc2"});
		var label_minus = $.UI.create("Label", {classes:['wsize','hsize','h6'], touchEnabled: false, text: "-"});
		var view_quantity_value = $.UI.create("View", {classes:['wsize'], height: 20,  top: 1, right:1, bottom:1, backgroundColor: "#3dabc2"});
		var label_quantity_value = $.UI.create("Label", {classes:['wsize','hsize','h6'], left:20, right:20, text: arr[i].quantity});
		var view_plus = $.UI.create("View", {index: i, operator: "+", width: 20, height: 20,  top: 1, right:1, bottom:1, backgroundColor: "#3dabc2"});
		var label_plus = $.UI.create("Label", {classes:['wsize','hsize','h6'], touchEnabled: false, text: "+"});

		view_minus.addEventListener("click", adjustQuantity);
		view_plus.addEventListener("click", adjustQuantity);
		image_delete.addEventListener("click", adjustQuantity);
		view_container.add(image_order);
		view_container.add(view_info);
		view_container.add(image_delete);
		view_info.add(label_name);
		view_info.add(label_retail_price);
		view_info.add(view_quantity);
		view_quantity.add(view_minus);
		view_quantity.add(view_quantity_value);
		view_quantity.add(view_plus);
		view_minus.add(label_minus);
		view_quantity_value.add(label_quantity_value);
		view_plus.add(label_plus);

		$.listing.add(view_container);
	};
}

function adjustQuantity(e){
	var cart_str = Ti.App.Properties.getString("cart") || "[]";
	var cart = JSON.parse(cart_str);
	console.log(e.source);
	if(e.source.operator == "+"){
		cart[e.source.index].quantity += 1;
		e.source.parent.children[1].children[0].text = cart[e.source.index].quantity;
	}else if(e.source.operator == "-" && cart[e.source.index].quantity > 0){
		cart[e.source.index].quantity -= 1;
		e.source.parent.children[1].children[0].text = cart[e.source.index].quantity;
	}else if(e.source.operator == "delete"){
		cart.splice(e.source.index, 1);
		e.source.parent.parent.remove(e.source.parent);
	}

	Ti.App.Properties.setString("cart", JSON.stringify(cart));
	Ti.App.fireEvent("refresh_cart");
	calculate_subtotal();
}

function checkout(e){
    var win = Alloy.createController("eorders/checkout", {}).getView();
	console.log(win);
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

function navTo(e){
	var win = Alloy.createController("eorders/detail", {id: e.source.package_id}).getView();
	console.log(win);
	if(OS_IOS){
		drawer.centerWindow.openWindow(win);
	}else{
		win.open();
	}
}

Ti.App.addEventListener("closeWindow", closeWindow);

function closeWindow(e){
	Ti.App.removeEventListener("closeWindow", closeWindow);
	$.win.close();
}
