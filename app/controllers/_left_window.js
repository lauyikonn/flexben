// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var layout = [{
		title: "HOME",
		isparent: false,
		target: "home" 
	}, 
	/*{
		title: "HEALTH PROFILE",
		isparent: false,
		target: "health_profile/index" 
	}, 
	{
		title : "MY ACCOUNT",
		isparent : true,
		opened : false,
		sub : [{
		    title : "PROFILE & SETTINGS",
		    target: "profile"
		}, {
		    title : "ELECTRONIC CARD",
		    target: "ecard"
		}]
	},
	{
		title: "PANEL CLINIC LOCATOR",
		target: "map/index",
		isparent: false,
	},	{
		title: "FEEDBACK",
		target: "feedback/index",
		isparent: false,
	},	*/{
		title: "LOGOUT",
		isparent: false,
	},
];


function init(){
	var tbr = [];
	for (var i=0; i < layout.length; i++) {
		console.log(layout[i].target);
		var row = $.UI.create("TableViewRow", {target: layout[i].target, isparent : layout[i].isparent, opened : layout[i].opened, sub: layout[i].sub, root: true, menu_title: layout[i].title});
		var view_container = $.UI.create("View", {classes:['wfill','hsize','padding']});
		var label_title = $.UI.create("Label", {classes:['wfill','hsize','h5', 'dark_green'], text: layout[i].title});
		view_container.add(label_title);
		row.add(view_container);
		tbr.push(row);
	};
	$.menu_list.setData(tbr);
}

init();

$.menu_list.addEventListener("click", function(e) {
	//Is this a parent cell?
	if (e.row.isparent) {
	    //Is it opened?
	    if (e.row.opened) {
	        for (var i = e.row.sub.length; i > 0; i = i - 1) {
	            $.menu_list.deleteRow(e.index + i);
	        }
	        e.row.opened = false;
	    } else {
	        //Add teh children.
	        var currentIndex = e.index;
	        for (var i = 0; i < e.row.sub.length; i++) {
	        	var row = $.UI.create("TableViewRow", {target: e.row.sub[i].target});
				var view_container = $.UI.create("View", {classes:['wfill','hsize','padding']});
				var label_title = $.UI.create("Label", {classes:['wfill','hsize','h6','bold'], color: "#06141c", text: e.row.sub[i].title});
				view_container.add(label_title);
				row.add(view_container);
	            $.menu_list.insertRowAfter(currentIndex, row);
	            currentIndex++;
	        }
	        e.row.opened = true;
	    }
	}
});

function navTo(e){
	if(e.rowData.target != null){
		var win = Alloy.createController(e.rowData.target).getView();
		if(OS_IOS && e.rowData.root){
			var nav_window = $.UI.create("NavigationWindow", {role: "centerWindow"});
			nav_window.window = win;
			Alloy.Globals.drawer.setCenterWindow(nav_window);
			Alloy.Globals.drawer.toggleLeftWindow();
		}else{
			Alloy.Globals.drawer.setCenterWindow(win);
			Alloy.Globals.drawer.toggleLeftWindow();
		}
	}else{
		console.log(e.rowData.menu_title);
		if(e.rowData.menu_title == "LOGOUT"){
			Ti.App.Properties.removeAllProperties();
			Alloy.Globals.drawer.close();
		}
	}
}


