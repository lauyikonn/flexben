var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var u_id = Ti.App.Properties.getString("u_id") || "";
var API = require('api');
function init(){
	$.win.add(loading.getView());
}
init();

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {
		console.log(forms_arr[i].id+" "+forms_arr[i].value);
		forms_arr[i].value = forms_arr[i].value.trim();
		if(forms_arr[i].required && forms_arr[i].value == ""){
			error_message += forms_arr[i].id+" cannot be empty\n";
		}
		if(typeof forms_arr[i].fieldType != "undefined" && forms_arr[i].fieldType == "email" && checkEmailFormat(forms_arr[i].value)){
			error_message += "Invalid "+forms_arr[i].id+" format\n";
		}
		if(!forms_arr[i].ignore){
			params[forms_arr[i].id] = forms_arr[i].value;
		}
	};
	if($.confirm_password.value != $.password.value){
		error_message += "Password does not match the confirm password";
	}
	if(error_message != ""){
		alert(error_message);
		return;
	}
	
	params["u_id"] = u_id;
	params["app_version"] = Titanium.App.version;
	params["device_os"] = Titanium.Platform.osname+" "+Titanium.Platform.version;
	params["device_model"] = Titanium.Platform.model+" "+Titanium.Platform.ostype;
	params["device_macaddress"] = Titanium.Platform.macaddress;
	 
	console.log(params);
	loading.start();
	API.callByPost({url: "api/registerDemoEmployee2", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?"Welcome to FlexBen":result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
		    		_.each(result.data, function(value, key){
					    Ti.App.Properties.setString(key, value);
					});
					var win = Alloy.createController("drawer").getView();
					win.open();
					args.nw.close();
					closeWindow();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function checkEmailFormat(email){
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var result = (email.match(mailformat))?false:true;
	return result;
}

function closeWindow(){
	$.win.close();
}
