var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var nw = (OS_IOS)?args.nw:$.win;
var API = require('api');
function init(){
	$.win.add(loading.getView());
}
init();

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {
		console.log(forms_arr[i].id+" "+forms_arr[i].value);
		forms_arr[i].value = forms_arr[i].value.trim();
		if(forms_arr[i].required && forms_arr[i].value == ""){
			error_message += forms_arr[i].id+" cannot be empty\n";
		}
		if(typeof forms_arr[i].fieldType != "undefined" && forms_arr[i].fieldType == "email" && checkEmailFormat(forms_arr[i].value)){
			error_message += "Invalid "+forms_arr[i].id+" format\n";
		}
		if(!forms_arr[i].ignore){
			params[forms_arr[i].id] = forms_arr[i].value;
		}
	};
	if(error_message != ""){
		alert(error_message);
		return;
	}
	
	loading.start();
	API.callByPost({url: "api/doForgotPassword", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?"Successful, Please check your email.":result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
					nw.close();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function checkEmailFormat(email){
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var result = (email.match(mailformat))?false:true;
	return result;
}

function closeWindow(){
	nw.close();
}
