var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var nw = (OS_IOS)?args.nw:$.win;
var API = require('api');
function init(){
	$.win.add(loading.getView());
}
init();

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {
		console.log(forms_arr[i].id+" "+forms_arr[i].value);
		if(forms_arr[i].required && forms_arr[i].value == ""){
			error_message += forms_arr[i].id+" cannot be empty\n";
		}
		params[forms_arr[i].id] = forms_arr[i].value;
	};
	if(error_message != ""){
		alert(error_message);
		return;
	}
	console.log(params);
	loading.start();
	API.callByPost({url: "api/registerDemoEmployee", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?"You have successfully registered":result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
		    		_.each(result.data, function(value, key){
					    Ti.App.Properties.setString(key, value);
					});
					var win = Alloy.createController("signup/signup2", {nw: nw}).getView();
					win.open();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function closeWindow(){
	//$.win.close();
	//$.win.close();
	//console.log(nw);
	nw.close();
}
