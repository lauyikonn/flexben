var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var u_id = Ti.App.Properties.getString("u_id") || "";
var API = require('api');
function camera_callback(event){
    console.log((640 / event.media.width)+" "+(640 / event.media.height));
    var new_height = (event.media.height <= event.media.width)?event.media.height*(640 / event.media.width):640;
    var new_width = (event.media.width <= event.media.height)?event.media.width*(640 / event.media.height):640;
    var blob = event.media;
    console.log(" "+event.media.width+" "+event.media.height);
    console.log(new_width+" "+new_height);
    blob = blob.imageAsResized(new_width, new_height);
    $.image_preview.image = blob;
    $.image_preview.parent.filedata = blob;
    $.image_preview.parent.attached = 1;
}

function init(){
    $.win.add(loading.getView());
    $.camera.init({callback: camera_callback, source: $.camera});
    //refresh();
}
init();

function loadComboBox(e){
	console.log(u_id+" uid");
	var indicator = $.UI.create("ActivityIndicator", {classes:['wsize','hsize'], style: Ti.UI.ActivityIndicatorStyle.DARK,});
	indicator.show();
	e.source.add(indicator);
	var params = {u_id: u_id};
	API.callByPost({url: e.source.url, params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(result.status == "success"){
				e.source.data = result.data;
			}else{
				e.source.data = [];
			}
		}, onfinish: function(){
			e.source.opacity = 1;
			e.source.touchEnabled = true;
			indicator.hide();
		}
	});
}

function hintTextFocusHack(e){
	console.log(e.source.hintText);
	if(e.source.value == e.source.hintText){
        e.source.value = "";
        e.source.color = "#ffffff";
    }
}

function blurAll(source){
	for(var a=0; a<$.forms.children.length; a++){
		if(typeof($.forms.children[a].blur) == "function" && source.id != $.forms.children[a].id){
			$.forms.children[a].blur();
		}
	}
}

function hintTextBlurHack(e){
	if(e.source.value==""){
        e.source.value = e.source.hintText;
        e.source.color = "#ffffff";
    }
}

function datePicker(e){
	var val_date = (typeof e.source.date != "undefined")?e.source.date:new Date();
	var view_container = $.UI.create("View", {classes:['wfill', 'hfill'], zIndex: 50,});
	var mask = $.UI.create("View",{
		classes:['wfill','hfill'],
		backgroundColor: "#80000000"
	});
	var view_box = $.UI.create("View", {classes:['wfill','hsize','vert'], 
	backgroundGradient:{
		type: 'linear',
		colors: [ { color: '#ffffff', offset: 0.0},{ color: '#67b6e1', offset: 0.4 }, { color: '#67b6e1', offset: 0.6 }, { color: '#ffffff', offset: 1.0 } ],
	}, zIndex: 50});
	var picker = $.UI.create("Picker", {
		type:Ti.UI.PICKER_TYPE_DATE,
		value: val_date,
		backgroundColor: "Transparent",
		//dateTimeColor: "#ffffff",
		top: 10,
	});
	var ok_button = $.UI.create("Button", {classes:['wfill'], borderRadius:0, height: 50, title: "Select a Date"});
	view_box.add(picker);
	view_box.add(ok_button);
	view_container.add(view_box);
	view_container.add(mask);
	$.win.add(view_container);
	
	mask.addEventListener("click", function(){ 
		$.win.remove(view_container);
	});
	
	ok_button.addEventListener("click", function(ex){
		var dd = picker.value.getDate();
		var mm = picker.value.getMonth()+1; 
		var yyyy = picker.value.getFullYear();
		e.source.value = yyyy+'-'+mm+'-'+dd;
		e.source.date = picker.value;
		e.source.children[0].text = mm+'/'+dd+'/'+yyyy;
		e.source.children[0].color = "#ffffff";
		$.win.remove(view_container);
	});
}

function popout(e){
	var options_arr = _.pluck(e.source.data, e.source.option_name);
	options_arr.push("Cancel");
	var dialog = Ti.UI.createOptionDialog({
		cancel: options_arr.length - 1,
		options: options_arr,
		selectedIndex: e.source.value || 0,
		title: e.source.children[0].text
	});
		
	dialog.show(); 
	dialog.addEventListener("click", function(ex){
		console.log(ex.index+" "+ex.cancel);
		if(ex.cancel != ex.index){
			e.source.children[0].text = options_arr[ex.index];
			e.source.value = e.source.data[ex.index][e.source.option_key];
			e.source.children[0].color = "#ffffff";
		}
	});
}

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {category: 2};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {
		if(!forms_arr[i].ignore){
			console.log(forms_arr[i].id+" "+forms_arr[i].value);
			if(forms_arr[i].required && forms_arr[i].value == ""){
				console.log(_.isUndefined(forms_arr[i].value)+" _.isEmpty(forms_arr[i].value)");
				error_message += forms_arr[i].hintText+" cannot be empty\n";
			}
			if(forms_arr[i].format == "photo" && forms_arr[i].attached){
				_.extend(params, {Filedata: forms_arr[i].filedata});
			}else{
				params[forms_arr[i].id] = forms_arr[i].value;
			}
		}
	};
	if(error_message != ""){
		alert(error_message);
		return;
	}
	params["u_id"] = u_id;
	console.log(params);
	loading.start();
	API.callByPost({url: "api/doRequestGL", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
			
			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?result.data:result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
					$.win.close();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function closeWindow(){
	$.win.close();
}
