var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh();
}
init();

function refresh(){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	console.log(u_id+" uid");
	if(u_id == ""){
		return;
	}
	var params = {u_id: u_id};
	API.callByPost({url: "api/getGLList", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(typeof result.data != "undefined" && result.data.length > 0){
				render_gl_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_gl_list(arr){
	console.log(arr.length+" arr.length");
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		var label_clinicName = $.UI.create("Label", {classes:['wfill','hsize','padding','h4'], text: arr[i].hospital});
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
		var label_add1 = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: "Type: "+arr[i].gl_category+"\n Issued Date: "+arr[i].date});
		var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
		var button_map = $.UI.create("Button", {width: "40%", left: 10, bottom:10,  title: "SHOW", target: arr[i].gl_link});
		
		view_container.add(label_clinicName);
		view_container.add(view_hr);
		view_container.add(label_add1);
		view_action.add(button_map);
		view_container.add(view_action);
		
		$.gl_listing.add(view_container);
		button_map.addEventListener("click", openURL);
		
	};
}

function openURL(e){
	var win = Alloy.createController("webview", {url: e.source.target, title: "Guarantee Letter"}).getView();
	if(OS_IOS){
		Alloy.Globals.drawer.centerWindow.openWindow(win);
	}else{
		win.open();	
	}
}

function closeWindow(e){
	$.win.close();
}


