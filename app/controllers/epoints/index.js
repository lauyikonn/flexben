var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh({});
}
init();

function refresh(e){
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: "api/getPointsList", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_list(result.data);
			}
			$.points.text = result.points || 0;
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_list(arr){
	$.listing.removeAllChildren();
	console.log(arr.length+" arr.length");
	var historySection = Ti.UI.createListSection({ headerTitle: 'Points History'});
    var point_data = [];
	for (var i=0; i < arr.length; i++) {
	    point_data.push({ title: {text: arr[i].title}, date: {text: arr[i].date}, points: {text: arr[i].points+" points"}});
	};
	historySection.setItems(point_data);
	$.listing.appendSection(historySection);
}

function closeWindow(e){
	$.win.close();
}

