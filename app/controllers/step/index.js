var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var model = Alloy.createCollection("steps");

function init(){
	$.win.add(loading.getView());
	var data = model.getData();
	console.log(data);
	$.step.text = (data.length > 0)?data[0].step_count:$.step.text;
}
init();

function closeWindow(e){
	$.win.close();
}
