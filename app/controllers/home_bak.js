// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var loading = Alloy.createController("loading");

function init(){
	$.win.add(loading.getView());
	render_user_info();
	//refresh();
}

init();

//init private function start

function render_user_info(){
	$.name.text = Ti.App.Properties.getString("name");
	$.company_logo.image =  Ti.App.Properties.getString("company_logo");
	$.company_name.text = Ti.App.Properties.getString("company_name");
	//$.enquiries_email.text =  "email: "+(Ti.App.Properties.getString("company_email") || Ti.App.Properties.getString("flexben_email"));
	//$.enquiries_contact.text =  "tel: "+(Ti.App.Properties.getString("company_contact") || Ti.App.Properties.getString("flexben_contact"));
	var master = "NRIC: "+Ti.App.Properties.getString("master") || "";
	$.empno.text = "Staff No: "+Ti.App.Properties.getString("empno") || "";
	$.location.text = "Subsidiary Location: "+Ti.App.Properties.getString("location") || "";
	$.master.text = master;// master.replace(/(.{4})/g, '$1  ');
}

//init private function end 

function refresh(){
	loading.start();
	var master = Ti.App.Properties.getString("master") || "";
	var params = {master: master};
	API.callByPost({url: "api/getStaffEntitlement", params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(typeof result.data != "undefined" && result.data.length > 0){
				render_entitlement(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

//refresh private function start

function render_entitlement(arr){
	for (var i=0; i < arr.length; i++) {
		var view_div = $.UI.create("View", {classes:['wfill', 'bg_blue'], top:10, height: 10});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize', 'h5', 'bold', 'blue', 'padding'], text: arr[i].name, bottom:0});
		
		if(i>0){
			$.ent_list.add(view_div);
		}
		$.ent_list.add(label_name);
		
		for (var j=0; j < arr[i].entitlement.length; j++) {
			var entitlement = arr[i].entitlement[j];
			var text_balance = (entitlement.limit_pa == -1)?"UNLIMITED":"Balance: "+entitlement.balance_pa+"/"+entitlement.limit_pa;
			var view_entitlement = $.UI.create("View", {classes:['wfill', 'hsize', 'vert', 'padding']});
			var view_ent_title = $.UI.create("View", {classes:['wfill', 'hsize'], bottom: 5});
			var label_category = $.UI.create("Label", {classes:['wsize','hsize','h5','bold','green'], left:0, text: entitlement.category});
			var label_balance = $.UI.create("Label", {classes:['wsize','hsize','h6'], right:0, text: text_balance});
			
			view_ent_title.add(label_category);
			view_ent_title.add(label_balance);
			view_entitlement.add(view_ent_title);
			console.log((entitlement.limit_pa - entitlement.balance_pa)+" "+entitlement.limit_pa);
			var percentage_usage = (entitlement.limit_pa == -1)?100:Math.round(( entitlement.balance_pa / entitlement.limit_pa)*100);
			var percentage_text = (entitlement.limit_pa == -1)?"UNLIMITED":percentage_usage+"% BALANCE";
			var text_color = (entitlement.limit_pa == -1 || percentage_usage > 27)?"white":"blue";
			var text_bg = (entitlement.limit_pa == -1 || percentage_usage > 10)?"bg_green":"bg_red";
			if(text_bg == "bg_green"){
				text_bg = (percentage_usage < 75)?"bg_yellow":"bg_green";
			}
			var view_ent_bar = $.UI.create("View", {classes:['wfill', 'hsize'], backgroundColor: "#ffffff"});
			var view_ent_barcolor = $.UI.create("View", {classes:[text_bg], left:0, height: 30, width: percentage_usage+"%"});
			var label_percentage = $.UI.create("Label", {classes:['wsize','hsize','bold', text_color], text: percentage_text, left: 10});
			
			view_ent_bar.add(view_ent_barcolor);
			view_ent_bar.add(label_percentage);
			view_entitlement.add(view_ent_bar);
			
			var view_ent_period = $.UI.create("View", {classes:['wfill', 'hsize']});
			var label_period = $.UI.create("Label", {classes:['wsize','hsize','h6'], text: "Period: From "+entitlement.start_date+" till "+entitlement.end_date, left: 0, top:5});
			
			view_ent_period.add(label_period);
			view_entitlement.add(view_ent_period);
			
			$.ent_list.add(view_entitlement);
		};
	};
}

//refresh private function end

function navTo(e){
	var win = Alloy.createController(e.source.target, e.source.param || {}).getView();
	var target = e.source.target;
	if(OS_IOS){
		drawer.centerWindow.openWindow(win);
	}else{
		win.open();	
	}
}
