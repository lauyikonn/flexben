var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var u_id = Ti.App.Properties.getString("u_id") || "";
console.log("vitality/submit");
var API = require('api');
function camera_callback(event, source){
	console.log("check callback");
	console.log((640 / event.media.width)+" "+(640 / event.media.height));
	var new_height = (event.media.height <= event.media.width)?event.media.height*(640 / event.media.width):640;
	var new_width = (event.media.width <= event.media.height)?event.media.width*(640 / event.media.height):640;
	var blob = event.media;

	console.log(" "+event.media.width+" "+event.media.height);
	console.log(new_width+" "+new_height);
	blob = blob.imageAsResized(new_width, new_height);
    var img = Ti.UI.createImageView({image: blob, height:Ti.UI.FILL, width: Ti.UI.SIZE, filedata:blob});
	//var img = $.UI.create("ImageView", {width: 120, classes:['hsize'], image: blob});
	if(source.parent.children[1].children.length > 0){
		source.parent.children[1].removeAllChildren();
	}
	source.parent.children[1].add(img);
	source.parent.attached = 1;
	API.callByPost({url: "api/doAddVitalityData", params: {u_id: u_id, type: source.vitality_type, date: $.date.value, Filedata: blob}}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);
		}, onfinish: function(){
			//e.source.opacity = 1;
			//e.source.touchEnabled = true;
			//indicator.hide();
		}
	});
}

function blurAll(source){
	for(var a=0; a<$.forms.children.length; a++){
		if(typeof($.forms.children[a].blur) == "function" && source.id != $.forms.children[a].id){
			$.forms.children[a].blur();
		}
	}
}

function init(){
	$.win.add(loading.getView());
	console.log("why no call init "+ $.overall.vitality_type);
	var moment = require('alloy/moment');
	//console.log(moment().format("DD/MM/YYYY"));
	$.date.value = moment().format("YYYY-MM-DD");
	$.date.children[0].text = moment().format("YYYY-MM-DD");
	$.overall.init({callback: camera_callback, source: $.overall});
	$.breakfast.init({callback: camera_callback, source: $.breakfast});
	$.lunch.init({callback: camera_callback, source: $.lunch});
	$.snack.init({callback: camera_callback, source: $.snack});
	$.water.init({callback: camera_callback, source: $.water});
	$.dinner.init({callback: camera_callback, source: $.dinner});

	refresh();
}
init();

function refresh(){
	var list_arr = $.listing.getChildren();
	for (var i=0; i < list_arr.length; i++) {
	  list_arr[i].children[1].removeAllChildren();
	};
	API.callByPost({url: "api/getVitalityData", params: {u_id:u_id, date: $.date.value}}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result.data);
			for (var i=0; i < result.data.length; i++) {
				var img = Ti.UI.createImageView({image: result.data[i].img_path, height:Ti.UI.FILL, width: Ti.UI.SIZE});
			  	eval("$."+result.data[i].type+".add(img)");
			};
		}
	});
}

function loadComboBox(e){
	console.log(u_id+" uid");
	var indicator = $.UI.create("ActivityIndicator", {classes:['wsize','hsize'], style: Ti.UI.ActivityIndicatorStyle.DARK,});
	indicator.show();
	e.source.add(indicator);
	var params = {u_id: u_id};
	API.callByPost({url: e.source.url, params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if(result.status == "success"){
				e.source.data = result.data;
			}else{
				e.source.data = [];
			}
		}, onfinish: function(){
			e.source.opacity = 1;
			e.source.touchEnabled = true;
			indicator.hide();
		}
	});
}

function hintTextFocusHack(e){
	console.log(e.source.hintText);
	if(e.source.value == e.source.hintText){
        e.source.value = "";
        e.source.color = "#ffffff";
    }
}

function hintTextBlurHack(e){
	if(e.source.value==""){
        e.source.value = e.source.hintText;
        e.source.color = "#ffffff";
    }
}

function datePicker(e){
	var val_date = (typeof e.source.date != "undefined")?e.source.date:new Date();
	var view_container = $.UI.create("View", {classes:['wfill', 'hfill'], zIndex: 50,});
	var mask = $.UI.create("View",{
		classes:['wfill','hfill'],
		backgroundColor: "#80000000"
	});
	var view_box = $.UI.create("View", {classes:['wfill','hsize','vert'],
	backgroundGradient:{
		type: 'linear',
		colors: [ { color: '#ffffff', offset: 0.0},{ color: '#67b6e1', offset: 0.4 }, { color: '#67b6e1', offset: 0.6 }, { color: '#ffffff', offset: 1.0 } ],
	}, zIndex: 50});
	var picker = $.UI.create("Picker", {
		//classes:['hsize'],
		height: (OS_ANDROID)?150:Ti.UI.SIZE,
		type:Ti.UI.PICKER_TYPE_DATE,
		value: val_date,
		color: "#ffffff",
		backgroundColor: "Transparent",
		//dateTimeColor: "#ffffff",
		top: 20,
		bottom: 20,
	});
	var ok_button = $.UI.create("Button", {classes:['wfill'], borderRadius:0, height: 50, title: "Select a Date"});
	view_box.add(picker);
	view_box.add(ok_button);
	view_container.add(view_box);
	view_container.add(mask);
	$.win.add(view_container);

	mask.addEventListener("click", function(){
		$.win.remove(view_container);
	});

	ok_button.addEventListener("click", function(ex){
		var dd = picker.value.getDate();
		var mm = picker.value.getMonth()+1;
		var yyyy = picker.value.getFullYear();
		e.source.value = yyyy+'-'+mm+'-'+dd;
		e.source.date = picker.value;
		e.source.children[0].text = yyyy+'-'+mm+'-'+dd;
		e.source.children[0].color = "#ffffff";
		$.win.remove(view_container);
		refresh();
	});
}

function popout(e){
	var options_arr = _.pluck(e.source.data, e.source.option_name);
	options_arr.push("Cancel");;
	var dialog = Ti.UI.createOptionDialog({
		cancel: (options_arr.length > 0)?options_arr.length - 1:0,
		options: options_arr,
		selectedIndex: e.source.value || 0,
		title: e.source.children[0].text
	});

	dialog.show();
	dialog.addEventListener("click", function(ex){
		console.log(ex.index+" "+ex.cancel);
		if((OS_IOS)?ex.cancel != ex.index:!ex.cancel){
			e.source.children[0].text = options_arr[ex.index];
			e.source.value = e.source.data[ex.index][e.source.option_key];
			e.source.children[0].color = "#ffffff";
		}
	});
}

function doSubmit(){
	var forms_arr = $.forms.getChildren();
	var params = {};
	var error_message = "";
	for (var i=0; i < forms_arr.length - 1; i++) {
		console.log(forms_arr[i].id+" "+forms_arr[i].value);
		if(forms_arr[i].required && forms_arr[i].value == ""){
			console.log(_.isUndefined(forms_arr[i].value)+" _.isEmpty(forms_arr[i].value)");
			error_message += forms_arr[i].hintText+" cannot be empty\n";
		}
		console.log(forms_arr[i].format+" "+forms_arr[i].attached);
		if(forms_arr[i].format == "photo"){
			var photo_arr = $.image_preview.getChildren();
			var files_arr = [];
			console.log(photo_arr.length+" how many photo");
			for (var j=0; j < photo_arr.length; j++) {
			  //files_arr.push(photo_arr[j].filedata);
			  var key = "Filedata"+((j)?j:"");
			  console.log(key);
			  console.log("_.extend(params, {"+key+": photo_arr[j].filedata})");
			  eval("_.extend(params, {"+key+": photo_arr[j].filedata})");
			  //_.extend(params, {key: photo_arr[j].filedata});
			};

		}else if(forms_arr[i].format == "photo" && !forms_arr[i].attached){
			error_message += "Please upload your receipt\n";
		}else{
			params[forms_arr[i].id] = forms_arr[i].value;
		}
	};

	if(error_message != ""){
		alert(error_message);
		return;
	}
	params["u_id"] = u_id;
	console.log(params);
	loading.start();
	API.callByPost({url: "api/doClaimSubmission", params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			console.log(result);

			var dialog = Ti.UI.createAlertDialog({
			    cancel: 1,
			    buttonNames: ['Ok'],
			    status: result.status,
			    message: (result.status == "success")?"Your claim has been successfully submitted":result.data.join("\n"),
			    title: (result.status == "success")?"Success":"Error"
		  	});
		  	dialog.addEventListener('click', function(e) {
		    	if(e.source.status == "success"){
					$.win.close();
				}
		  	});
		  	dialog.show();
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function closeWindow(){
	$.win.close();
}

var counter = 0;

$.win.addEventListener("postlayout", function(){
});
