var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh({url: "api/getRedemptionList"});
}
init();

function refresh(e){
    var source = (typeof e.source != "undefined")?e.source:e;
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	if(u_id == ""){
		return;
	}
	
	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: source.url, params: params}, { 
		onload: function(responseText){
			var result = JSON.parse(responseText);
			$.listing.removeAllChildren();
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_list(arr){
	console.log(arr.length+" arr.length");
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		var image_redeem = $.UI.create("ImageView", {classes:['wfill','hsize'], bottom:10, image: arr[i].image});
		var label_clinicName = $.UI.create("Label", {classes:['wfill','hsize','padding'], text: arr[i].name});
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});
		//var text_address = arr[i].add1+" "+arr[i].add2+" "+arr[i].city+" "+arr[i].state;
		var label_point = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], textAlign: "right", top:0, text: arr[i].points+" POINTS"});
		var view_action = $.UI.create("View", {classes:['wfill','hsize','horz']});
		var button_map = $.UI.create("Button", {width: "40%", left: 10, bottom:10,  title: "REDEEM",  redemption_id: arr[i].id});
		
		view_container.add(image_redeem);
		view_container.add(view_hr);
		view_container.add(label_clinicName);
		view_container.add(label_point);
		view_action.add(button_map);
		view_container.add(view_action);
		
		$.listing.add(view_container);
		
		button_map.addEventListener("click", doRedeem);
		
	};
}

function doRedeem(e){
    loading.start();
    var u_id = Ti.App.Properties.getString("u_id") || "";
    var source = (typeof e.source != "undefined")?e.source:e;
    var params = {u_id: u_id, item_id: source.redemption_id, quantity: 1};
    API.callByPost({url: "api/doRedeem", params: params}, {
        onload: function(responseText){
            var result = JSON.parse(responseText);
            alert(result.data);
        }, onfinish: function(){
            loading.finish();
        }
    });
}

function closeWindow(e){
	$.win.close();
}

