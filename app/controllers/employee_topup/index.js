var args = arguments[0] || {};
var loading = Alloy.createController("loading");
var API = require('api');
function init(){
	$.win.add(loading.getView());
	refresh({});
}
init();

function refresh(e){
	var url = "api/getEmployeeTopup";
	loading.start();
	var u_id = Ti.App.Properties.getString("u_id") || "";
	console.log(u_id+" uid"+url);
	if(u_id == ""){
		return;
	}

	var params = {u_id: u_id};
	if(typeof e.value != "undefined"){
		_.extend(params, {search: e.value});
	}
	API.callByPost({url: url, params: params}, {
		onload: function(responseText){
			var result = JSON.parse(responseText);
			if((typeof result.data != "undefined" && result.data.length > 0) || typeof params.search != "undefined"){
				render_list(result.data);
			}
		}, onfinish: function(){
			loading.finish();
		}
	});
}

function render_list(arr){
	$.clinic_list.removeAllChildren();
	console.log(arr.length+" arr.length");
	if(arr.length <= 0){
	    $.clinic_list.add($.UI.create("Label", {classes:['wfill','hsize','h4','padding'], text: "No Result found"}));
	}
	for (var i=0; i < arr.length; i++) {
		var view_container = $.UI.create("View", {classes:['wfill','hsize','vert','padding','bg_light_blue'], bottom: 0});
		var label_name = $.UI.create("Label", {classes:['wfill','hsize','padding','h4'], text: arr[i].insured_name});
		var view_hr = $.UI.create("View", {classes:['hr', 'bg_blue'], left: 10, right:10});

		var label_package_title = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], bottom:0, text: "PACKAGE"});
        var label_package = $.UI.create("Label", {classes:['wfill','hsize','padding'], bottom:0, top:0, text: arr[i].package_name});
        var view_package = $.UI.create("View", {classes:['wfill','hsize', 'vert'], left:0, width: "69%"});
        view_package.add(label_package_title);
        view_package.add(label_package);

				var label_options_title = $.UI.create("Label", {classes:['wsize','hsize','padding','bold'], bottom:0, text: "OPTIONS"});
				var label_options = $.UI.create("Label", {classes:['wsize','hsize','padding'], bottom:0, top:0,text: arr[i]. options});
				var view_options = $.UI.create("View", {classes:['hsize', 'wsize', 'vert'], left: "50%"});
				view_options.add(label_options_title);
				view_options.add(label_options);

				var label_plan_title = $.UI.create("Label", {classes:['wsize','hsize','padding','bold'], bottom:0, text: "PLAN"});
        var label_plan = $.UI.create("Label", {classes:['wsize','hsize','padding'], bottom:0, top:0,text: arr[i].plan});
        var view_plan = $.UI.create("View", {classes:['hsize', 'wsize', 'vert'], right:0});
        view_plan.add(label_plan_title);
        view_plan.add(label_plan);

				var view_two_col_2 = $.UI.create("View", {classes:['wfill','hsize']});
				view_two_col_2.add(view_package);
				view_two_col_2.add(view_options);
				view_two_col_2.add(view_plan);

				var label_policy_no_title = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], bottom:0, text: "POLICY NO."});
				var label_policy_no = $.UI.create("Label", {classes:['wfill','hsize','padding'], top:0,text: arr[i].policy_no});
				var view_policy_no = $.UI.create("View", {classes:['wfill','hsize', 'vert'], left:0, width: "49%"});
				view_policy_no.add(label_policy_no_title);
				view_policy_no.add(label_policy_no);

        var label_effective_date_title = $.UI.create("Label", {classes:['wsize','hsize','padding','bold'], bottom:0, text: "EFFECTIVE DATE"});
        var label_effective_date = $.UI.create("Label", {classes:['wsize','hsize','padding'], top:0,text: arr[i].effective_date});
        var view_effective_date = $.UI.create("View", {classes:['hsize','wsize', 'vert'], left: "50%"});
        view_effective_date.add(label_effective_date_title);
        view_effective_date.add(label_effective_date);

        var view_two_col = $.UI.create("View", {classes:['wfill','hsize']});
				view_two_col.add(view_policy_no);
				view_two_col.add(view_effective_date);

				var label_overall_limit_title = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], top:0, bottom:0, text: "OVERALL LIMIT"});
        var label_overall_limit = $.UI.create("Label", {classes:['wfill','hsize','padding'], top:0,text: arr[i].overall_limit});
        var view_overall_limit = $.UI.create("View", {classes:['hsize','wfill', 'vert']});
        view_overall_limit.add(label_overall_limit_title);
        view_overall_limit.add(label_overall_limit);

				var label_lifetime_limit_title = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], top:0, bottom:0, text: "LIFETIME LIMIT"});
        var label_lifetime_limit = $.UI.create("Label", {classes:['wfill','hsize','padding'], top:0,text: arr[i].lifetime_limit});
        var view_lifetime_limit = $.UI.create("View", {classes:['hsize','wfill', 'vert']});
        view_lifetime_limit.add(label_lifetime_limit_title);
        view_lifetime_limit.add(label_lifetime_limit);

				var label_deductible_limit_title = $.UI.create("Label", {classes:['wfill','hsize','padding','bold'], top:0, text: "DEDUCTIBLE AMOUNT"});
        var label_deductible_limit = $.UI.create("Label", {classes:['wfill','hsize','padding'], top:0,text: arr[i].deductible_amount});
        var view_deductible_limit = $.UI.create("View", {classes:['hsize','wfill', 'vert']});
        view_deductible_limit.add(label_deductible_limit_title);
        view_deductible_limit.add(label_deductible_limit);

		view_container.add(label_name);
		view_container.add(view_hr);
		view_container.add(view_two_col_2);
		view_container.add(view_two_col);
		view_container.add(view_overall_limit);
		view_container.add(view_lifetime_limit);
		view_container.add(view_deductible_limit);


		$.clinic_list.add(view_container);
	};
}

function closeWindow(e){
	$.win.close();
}
